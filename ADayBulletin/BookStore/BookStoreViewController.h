//
//  BookStoreViewController.h
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/18/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApiUtils.h"
@interface BookStoreViewController : UIViewController <ApiUtilsDelegate>
@property (weak, nonatomic) IBOutlet UIView *calendatContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calendarContentViewWidth;
@property (weak, nonatomic) IBOutlet UIScrollView *calendarScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *shelfScroll;
@property (weak, nonatomic) IBOutlet UIView *shelfContentview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shelfViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *moreImageView;


@end
