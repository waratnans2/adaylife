//
//  BookStoreViewController.m
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/18/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//
#import <MBProgressHUD/MBProgressHUD.h>
#import "BookStoreViewController.h"
#import "ApiUtils.h"
#import "BookCover.h"
#import "DBmanager.h"
#import "Book.h"
#import "MyDownloadViewController.h"
@interface BookStoreViewController ()
{
    NSMutableArray * yearsArray;
    ApiUtils * myApi;
    NSMutableArray * bookArray;
    NSArray *originX;
    NSString * currentYear;
    NSMutableArray *childViewControllerArray;
}

@property (nonatomic,retain) DBManager * dbMgr;
@property (nonatomic, strong) UISegmentedControl *mySegmentedControl;

@end

@implementation BookStoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(DBManager*)dbMgr
{
    if(_dbMgr == nil){
        _dbMgr = [[DBManager alloc]init];
    }
    return _dbMgr;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self createYearScroll];
    myApi = [[ApiUtils alloc]init];
    childViewControllerArray = [[NSMutableArray alloc]init];
    myApi.delegate = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

//    [myApi requestShelfYear];
    [myApi requestBookShelfByYear:@"ADBLife"];
//  [myApi requestBookDetail:@"/ADAY0159" year:@"2013"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveDownloadFileFinishNotification:)
                                                 name:@"finishDownloadPDFFileForMainNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveDownloadTempFileNotification:)
                                                 name:@"finishDownloadTempFileForMainNotification"
                                               object:nil];
    
}
#pragma mark notification
- (void) receiveDownloadFileFinishNotification:(NSNotification *) notification
{
    
    if([notification.name isEqualToString:@"finishDownloadPDFFileForMainNotification"]){
        
        NSDictionary* userInfo = notification.userInfo;
        
        int pageIndex = [[userInfo valueForKey:@"pageIndex"] integerValue];
        Book *bookObj = [userInfo valueForKey:@"Book"];
        
        NSLog(@"MAIN Download PDF %d of %@ ",pageIndex, bookObj.pageCount);
        
        bookObj.pageDownloaded = [NSString stringWithFormat:@"%d",pageIndex];
        
        if(pageIndex == [bookObj.pageCount intValue]){
            bookObj.state = [NSString stringWithFormat:@"3"];
        }
        [self.dbMgr updateDBBook:bookObj];
    }
}

- (void) receiveDownloadTempFileNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    
    if([notification.name isEqualToString:@"finishDownloadTempFileForMainNotification"]){
        
        NSDictionary* userInfo = notification.userInfo;
        
        int pageIndex = [[userInfo valueForKey:@"pageIndex"] integerValue];
        Book *bookObj = [userInfo valueForKey:@"Book"];
        
        NSLog(@"MAIN Download %d of %@ ",pageIndex, bookObj.pageCount);
        
        if(pageIndex == [bookObj.pageCount intValue]){
            //downloading state
            bookObj.state = [NSString stringWithFormat:@"2"];
            [self.dbMgr updateDBBook:bookObj];
        }
    }
}

#pragma mark resume download
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self resumeDownload];
    
}

-(void)resumeDownload
{
    
    NSMutableArray * bookForResume = [self.dbMgr fetchBook:@"state !=3"];
    NSLog(@"book for resume %d",[bookForResume count]);
    
    for (Book *bookObj in bookForResume) {
        
        NSString* downloadPDFFileNoti = [NSString stringWithFormat:@"finishDownloadPDFFileForBook%@",bookObj.bookName];
        NSString * downloadTempFileNoti = [NSString stringWithFormat:@"finishDownloadTempFileForBook%@",bookObj.bookName];
        
        int pageCount = [bookObj.pageCount intValue];
        if([bookObj.state isEqualToString:@"1"]){
            
            for (int i = 1; i<= pageCount; i++) {
                [myApi downloadTempFile:bookObj.filePath  year:bookObj.year fileName:bookObj.tempFile index:i notificationName:downloadTempFileNoti book:bookObj];
            }
        }
        
        int bookDownloaded = ([bookObj.pageDownloaded intValue]>1)?[bookObj.pageDownloaded intValue]:1;
        
        for (int i = bookDownloaded; i<= pageCount; i++) {
            [myApi downloadFile:bookObj.filePath year:bookObj.year fileName:bookObj.fileName index:i notificationName:downloadPDFFileNoti book:bookObj];
        }
        
        [myApi nextOperation];
    }
}

#pragma mark book shelf
- (void)adjustBook
{
    float coverWidth,coverHeight;
    float spaceMagin, spaceHeight;
    float shelfBar,shelfBarWidth,shelfBarHeight;
    int nRow,nPerPage;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        originX = [[NSArray alloc] initWithObjects:@"24", @"166", @"511", nil]; // 168
        coverWidth = 130;
        coverHeight = 230;
        spaceHeight = 240;
        spaceMagin = 15;
        nRow = 2;
        nPerPage = 2;
        shelfBar = 144;
        shelfBarHeight = 32;
        shelfBarWidth = 286;
    }
    else{
        originX = [[NSArray alloc] initWithObjects:@"34", @"272", @"511", nil];
        coverWidth = 207;
        coverHeight = 347;
        spaceHeight = 340;
        spaceMagin = 40;
        nRow = 3;
        nPerPage = 6;
        shelfBar = 199;
        shelfBarHeight = 64;
        shelfBarWidth = 734;
    }
    if ([bookArray count] > 0) {
        for (int i=0; i<[bookArray count]; i++)
        {
//            NSMutableDictionary *issueIndex = [[NSMutableDictionary alloc] initWithDictionary:[self.issueCatalog objectAtIndex:i]];
//            NSLog(@"ISSUE: %@",[issueIndex objectForKey:@"issue"]);
//            
//            BookCover *tempCoverView = (LeatherCover*)[scrollView viewWithTag:3000+i];
//
//            if (tempCoverView) {
//                NSLog(@"HAVE");
//            }
//            else
//            {
                //694 64
            
                BookCover *coverView = [[BookCover alloc] init];
                coverView.bookData = bookArray[i];
                coverView.shelfYear = currentYear;
//                coverView.delegate = self;
//                NSLog(@"Type: %@",type);
//                coverView.type = type;
                //                coverView.type = @"Booklet";
                
                coverView.view.tag = 3000+i;
                coverView.view.frame = CGRectMake([[originX objectAtIndex:i%nRow] floatValue], spaceMagin+(spaceHeight* (int)(i/nRow)), coverWidth, coverHeight);
            
//                coverView.detailDict = issueIndex;
                //                coverView.alreadyBuy = NO;
                //                coverView.loading = NO;
            

            if(i%nRow==0){
//                UIImageView * shelfImage = [[UIImageView alloc]initWithFrame:CGRectMake(17,coverView.view.frame.origin.y + shelfBar, shelfBarWidth, shelfBarHeight)];
//                [shelfImage setImage:[UIImage imageNamed:@"shelf ipad-1"]];
//                
//                [_shelfContentview addSubview:shelfImage];
                CGRect frame = CGRectMake(17,coverView.view.frame.origin.y + shelfBar, shelfBarWidth, 1);
                UIView * shelfBar = [[UIView alloc]initWithFrame:frame];
                [shelfBar setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:0.8]];
                [_shelfContentview addSubview:shelfBar];
                
            }
            
            [_shelfContentview addSubview:coverView.view];

            [self addChildViewController:coverView];
            [coverView didMoveToParentViewController:self];
            [childViewControllerArray addObject:coverView];


            }
        
            int row = ([bookArray count]%nRow == 0) ? 0 : 1;
            _shelfViewHeight.constant = ((int)[bookArray count]/nRow+ row) * spaceHeight + 50;
            [_shelfScroll setContentOffset:CGPointZero];

        }
    
        if([bookArray count] >6){[_moreImageView setHidden:NO];}else{[_moreImageView setHidden:YES];}
}

- (void)cleanView
{
    for (BookCover * controller in childViewControllerArray) {
        
        [controller willMoveToParentViewController:nil];
        [controller.view removeFromSuperview];
        [controller removeFromParentViewController];
        
    }
    [childViewControllerArray removeAllObjects];
    
    for (UIView *view in [_shelfContentview subviews]) {
        [view removeFromSuperview];
    }

}

#pragma mark calendar scroll
-(void)createYearScroll
{
    
    CGRect myFrame;
    UIFont *font;
    NSDictionary *attributes;
    
    
    //frame for the segemented button
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        myFrame = CGRectMake(5.0f, 0.0f, 310.0f, 29.0f);
        font = [UIFont boldSystemFontOfSize:14.0f];
        attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
        
    }else{
        myFrame = CGRectMake(10.0f, 0.0f, 748.0f, 50.0f);
        font = [UIFont boldSystemFontOfSize:18.0f];
        attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
        
    }
   
    //create an intialize our segmented control
    self.mySegmentedControl = [[UISegmentedControl alloc] initWithItems:yearsArray];

    //set the size and placement
    self.mySegmentedControl.frame = myFrame;
    [self.mySegmentedControl setTitleTextAttributes:attributes
                                           forState:UIControlStateNormal];
    //default the selection to second item
    [self.mySegmentedControl setSelectedSegmentIndex:0];
    [self.mySegmentedControl setTintColor:[UIColor colorWithRed:0.23 green:0.23 blue:0.23 alpha:1]];

//    //attach target action for if the selection is changed by the user
    [self.mySegmentedControl addTarget:self
                                action:@selector(touchYear:)
                      forControlEvents:UIControlEventValueChanged];
    
    //add the control to the view
    [self.calendatContentView addSubview:self.mySegmentedControl];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     
     if ([segue.identifier isEqualToString:@"myDownload"])
     {
         MyDownloadViewController *dest = [segue destinationViewController];
         dest.yearsArray = yearsArray.mutableCopy;

     }
 }
 

#pragma mark calendar button
- (void)touchYear:(UISegmentedControl *)paramSender
{
    NSInteger selectedIndex = [paramSender selectedSegmentIndex];
    
    currentYear = [paramSender titleForSegmentAtIndex:selectedIndex];
    
//    [self setButtonTo:currentYear];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [myApi requestBookShelfByYear:currentYear];
    
}

- (void)setButtonTo:(NSString*)title
{
    UIButton *selectButton = [[UIButton alloc] init];
    for (int i=0; i<[yearsArray count]; i++)
    {
        for (UIView *view in _calendatContentView.subviews) {
            if ([view isKindOfClass:[UIButton class]])
            {
                UIButton *button = (UIButton*)view;
                [button setTitleColor:[UIColor colorWithRed:81.0/255.0 green:72/255.0 blue:67.0/255.0 alpha:1] forState:UIControlStateNormal];
                
                if ([button.titleLabel.text isEqualToString:title]) {
                    selectButton = button;
                }
            }
        }
    }
    
    [selectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

#pragma mark api response protocol
- (void) processCompleted:(id)data api:(int)api success:(BOOL)success;
{
    if(!success){ NSLog(@"FAIL");  [MBProgressHUD hideHUDForView:self.view animated:YES];}
    if(api == API_REQUEST_SHELF_YEAR){
        yearsArray = [data valueForKey:@"item"];
        currentYear = [yearsArray firstObject];
        NSLog(@"%@ current year %@",yearsArray,currentYear);
        [self createYearScroll];
        [self setButtonTo:currentYear];
        [myApi requestBookShelfByYear:currentYear];

    }
    if(api == API_REQUEST_BOOKSHELF_BY_YEAR){
        if([data isKindOfClass:[NSMutableArray class]]){
            bookArray = [(NSMutableArray*)data copy];
            [self cleanView];
            [self adjustBook];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }
}

#pragma mark -
- (void) showMoreButton:(BOOL)show
{

    if (show) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        _moreImageView.alpha = 1.0;
        [UIView commitAnimations];
        
    }
    else{
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        _moreImageView.alpha = 0.0;
        [UIView commitAnimations];
        
    }
}

- (void)checkPage
{
    CGFloat pageHeight = _shelfScroll.frame.size.height;
    CGFloat contentHeight = _shelfScroll.contentSize.height;
    
    int page = floor((_shelfScroll.contentOffset.y - pageHeight / 2) / pageHeight) + 2;
    
    if ( (contentHeight - (pageHeight*page)) > pageHeight)
    {
        //NSLog(@"MORE");
        [self showMoreButton:YES];
    }
    else{
        //NSLog(@"NONE");
        [self showMoreButton:NO];
    }
}

#pragma mark - UIScrollView Delegate

- (void) scrollViewDidScroll:(UIScrollView *)scroll
{
    [self checkPage];
}


-(IBAction)closeView:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(IBAction)openFanPage:(id)sender
{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile?id=267847432346"]])
        NSLog(@"%@%@",@"Failed to open url:",@"fb://profile?id=267847432346");
}
@end



