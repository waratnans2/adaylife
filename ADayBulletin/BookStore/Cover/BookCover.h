//
//  BookCover.h
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/18/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Book.h"
@interface BookCover : UIViewController
{
    IBOutlet UIView *buyingView;
    IBOutlet UIView *loadingView;
    
    IBOutlet UIImageView *indicatorImageView;
    IBOutlet UIImageView *tailImageView; // 140
    
    IBOutlet UIImageView *coverImage;
    IBOutlet UIImageView *dropImage;
    IBOutlet UIImageView *highLightImage;
    IBOutlet UIButton *buyButton;
    IBOutlet UIButton *downloadButton;
    IBOutlet UIButton *previewButton;
    IBOutlet UIButton *freeBookletButton;
    IBOutlet UIButton *coverButton;

    IBOutlet UIButton *viewBookletButton;
    IBOutlet UIView *viewButton;
    
    IBOutlet UIButton *viewDownloading;
    IBOutlet UILabel *lblLoading;
}

@property (strong ,nonatomic) NSMutableDictionary * bookData;
@property (strong ,nonatomic) NSMutableDictionary * bookDetail;
@property (strong ,nonatomic) Book * bookObj;
@property (weak ,nonatomic) IBOutlet UILabel *bookname;
@property (nonatomic) BOOL libraryPage;
@property (strong ,nonatomic) NSString * shelfYear;

@end
