//
//  BookCover.m
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/18/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "BookCover.h"
#import "ApiUtils.h"
#import "Book.h"
#import "DBManager.h"
#import "DAProgressOverlayView.h"
#import "ReaderViewController.h"

@interface BookCover ()<ApiUtilsDelegate,ReaderViewControllerDelegate>
{
    NSString * downloadTempFileNoti;
    NSString * downloadPDFFileNoti;
    DAProgressOverlayView *progressOverlayView;
}
@property (nonatomic,retain) ApiUtils * apiUtils;
@property (nonatomic,retain) DBManager * dbMgr;

@end

@implementation BookCover
@synthesize bookObj;
@synthesize bookname;
@synthesize libraryPage;

-(id)init
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self = [super initWithNibName:@"LeatherCover_iphone" bundle:nil];
    }else{
        self = [super initWithNibName:@"LeatherCover" bundle:nil];
    }


    
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createNotiView];

    if(!bookObj){
        bookObj = [[self.dbMgr fetchBook:[NSString stringWithFormat:@"bookName = '%@'", [_bookData valueForKey:@"issue"]]] firstObject];
        bookname.text = [NSString stringWithFormat:@"%@ %@",[_bookData valueForKey:@"issue"],[_bookData valueForKey:@"pubDate"]];
        NSURL *thumb = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",host,_bookData[@"thumbnail"]]];
        NSLog(@"thumb %@",thumb);
        [coverImage setImageWithURL:thumb placeholderImage:[self imageWithColor:[UIColor colorWithWhite:1.0 alpha:0.4]]];
    }else{
        bookname.text = [NSString stringWithFormat:@"%@",bookObj.bookName];
        [coverImage setImage:[UIImage imageWithData:bookObj.thumbNail]];
    }
    
    [coverImage addSubview:progressOverlayView];
    
    if(bookObj){
        
        NSLog(@"%@ state:%@ pagedownload:%@/%@",bookObj.bookName,bookObj.state,bookObj.pageDownloaded,bookObj.pageCount);
     
        if([bookObj.state isEqualToString:@"1"]){
            [self showPrepareDownloadingOutlet];
        }else if([bookObj.state isEqualToString:@"2"]) {
            
            [self updateProgressView:(float)[bookObj.pageDownloaded intValue]/[bookObj.pageCount intValue]];
            [self showDownloadingOutlet];
            
        }else if ([bookObj.state isEqualToString:@"3"]){
            [self hideDownloadingOutlet];
        }
        
    }
    
}
-(void)createNotiView
{
    progressOverlayView = [[DAProgressOverlayView alloc] initWithFrame:coverImage.bounds];
    progressOverlayView.hidden = YES;
    if(!bookObj){
        downloadPDFFileNoti = [NSString stringWithFormat:@"finishDownloadPDFFileForBook%@",[_bookData valueForKey:@"issue"]];
        downloadTempFileNoti = [NSString stringWithFormat:@"finishDownloadTempFileForBook%@",[_bookData valueForKey:@"issue"]];
        
    }else{
        downloadPDFFileNoti = [NSString stringWithFormat:@"finishDownloadPDFFileForBook%@",bookObj.bookName];
        downloadTempFileNoti = [NSString stringWithFormat:@"finishDownloadTempFileForBook%@",bookObj.bookName];
        
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveDownloadTempFileNotification:)
                                                 name:downloadTempFileNoti
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveDownloadFileFinishNotification:)
                                                 name:downloadPDFFileNoti
                                               object:nil];
}
-(ApiUtils*)apiUtils
{
    if(_apiUtils == nil){
        _apiUtils = [[ApiUtils alloc]init];
        _apiUtils.delegate = self;
    }
    return _apiUtils;
}

-(DBManager*)dbMgr
{
    if(_dbMgr == nil){
        _dbMgr = [[DBManager alloc]init];
    }
    return _dbMgr;

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)showPrepareDownloadingOutlet
{
    progressOverlayView.hidden = NO;
    freeBookletButton.hidden = YES;
    lblLoading.hidden = NO;
}
-(void)showDownloadingOutlet
{
    
    if(libraryPage){
        coverButton.hidden = NO;
    }else{
        viewBookletButton.hidden = NO;
        
    }
    
    progressOverlayView.hidden = NO;
    freeBookletButton.hidden = YES;
    lblLoading.hidden = YES;
}

-(void)hideDownloadingOutlet
{

    if(libraryPage){
        coverButton.hidden = NO;
    }else{
        viewBookletButton.hidden = NO;

    }
    freeBookletButton.hidden = YES;
    lblLoading.hidden = YES;

}

- (void) receiveDownloadFileFinishNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:downloadPDFFileNoti])
    {
        NSDictionary* userInfo = notification.userInfo;
        
        int pageIndex = [[userInfo valueForKey:@"pageIndex"] integerValue];
        
        NSLog(@"Download PDF %d of %@ ",pageIndex, bookObj.pageCount);
        
        bookObj.pageDownloaded = [NSString stringWithFormat:@"%d",pageIndex];
        
        float progressValue = (float)pageIndex/[bookObj.pageCount intValue];
        [self updateProgressView:progressValue];
        
        if(pageIndex == [bookObj.pageCount intValue]){
            
            [self hideDownloadingOutlet];
            
        }
        
//        [self.dbMgr updateDBBook:bookObj];
    }
    
}

- (void) receiveDownloadTempFileNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:downloadTempFileNoti])
    {
        NSDictionary* userInfo = notification.userInfo;
        
        int pageIndex = [[userInfo valueForKey:@"pageIndex"] integerValue];
        
        NSLog(@"Download %d of %@ ",pageIndex, bookObj.pageCount);

        if(pageIndex == [bookObj.pageCount intValue]){
             //downloading state
//            bookObj.state = [NSString stringWithFormat:@"2"];
            [self showDownloadingOutlet];
//            [self.dbMgr updateDBBook:bookObj];
        }
    }
}

#pragma mark -
- (IBAction)downloadBook:(id)sender {
    
//    NSLog(@"test: %@",_bookData);

    [self showPrepareDownloadingOutlet];
    [sender setEnabled:NO];
    [self.apiUtils requestBookDetail:[_bookData valueForKey:@"bookdir"] year:_shelfYear];
    
}

- (IBAction)readBook:(id)sender {

    //test m
    ReaderDocument *pdfDoc = [[ReaderDocument alloc]init];
    pdfDoc.bookName = bookObj.bookName;

    pdfDoc.filePath = [[ReaderDocument documentsPath] stringByAppendingString:bookObj.filePath]; //folder path
    pdfDoc.pageCount = [NSNumber numberWithInt:[bookObj.pageCount intValue]];
    pdfDoc.tempFile = bookObj.tempFile;
    pdfDoc.pageNumber = [NSNumber numberWithInt:[bookObj.pageNumber intValue]];
    pdfDoc.pageDownloaded = [NSNumber numberWithInt:[bookObj.pageDownloaded intValue]]; // check if download all of file
    pdfDoc.pdfFileName = bookObj.fileName;
    NSIndexSet *setFromData= [NSKeyedUnarchiver unarchiveObjectWithData: bookObj.bookmarks];
    pdfDoc.bookmarks  = [setFromData mutableCopy];
    
    NSLog(@"path %@",pdfDoc.filePath);
    
    ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:pdfDoc];
    
    readerViewController.delegate = self; // Set the ReaderViewController delegate to self
    
    [self.navigationController pushViewController:readerViewController animated:YES];

//		readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//		readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    
//		[self presentViewController:readerViewController animated:YES completion:NULL];
    
}

-(void)updateProgressView:(float)value
{
    progressOverlayView.hidden = NO;
    progressOverlayView.progress = value;

}

-(void)prepareForDownload
{
    int totalpage = [bookObj.pageCount intValue];
    progressOverlayView.hidden = NO;

    for (int i=1; i <= totalpage; i++) {
        
        [self.apiUtils downloadTempFile:bookObj.filePath  year:_shelfYear fileName:bookObj.tempFile index:i notificationName:downloadTempFileNoti book:bookObj] ;
    }
}
-(void)downloadBookPDF
{
    int totalpage = [bookObj.pageCount intValue];
    
    for (int i=1; i <= totalpage; i++) {
        [self.apiUtils downloadFile:bookObj.filePath year:_shelfYear fileName:bookObj.fileName index:i notificationName:downloadPDFFileNoti book:bookObj];
    }
}

-(void)addToCoreData
{
    bookObj = [self.dbMgr getSchemaBook];
    bookObj.bookName = [_bookData valueForKey:@"issue"];
    bookObj.filePath = [_bookData valueForKey:@"bookdir"];
    bookObj.tempFile = [_bookDetail valueForKey:@"tempfile"];
    bookObj.pageCount = [_bookDetail valueForKey:@"totalpage"];
    bookObj.pubDate = [self convertMonthToNumber:[_bookData valueForKey:@"pubDate"]];
    bookObj.pageNumber = @"1";
    bookObj.pageDownloaded = @"0";
    bookObj.state = @"1";
    bookObj.year = _shelfYear;
//    obj.password
    bookObj.fileName = [_bookDetail valueForKey:@"filename"];
//  obj.bookmarks
    bookObj.thumbNail = UIImagePNGRepresentation(coverImage.image);
//    obj.cover = _book.cover;
    [self.dbMgr insertDBBook:bookObj];
}

#pragma mark api response protocol
- (void) processCompleted:(id)data api:(int)api success:(BOOL)success;
{
    if(!success){ NSLog(@"FAIL");}
    
    if(api == API_REQUEST_BOOKDETAIL){
        NSLog(@"%@",data);
        _bookDetail = data;
        
        [self addToCoreData];
        [self prepareForDownload];
        [self downloadBookPDF];
    }
}

#pragma mark ReaderViewControllerDelegate methods
- (void)dismissReaderViewController:(ReaderViewController *)viewController bookMarks:(NSData *)bookmarks currentPage:(NSNumber *)page
{
    bookObj.pageNumber = [page stringValue];
    bookObj.bookmarks = bookmarks;
    [self.dbMgr updateDBBook:bookObj];
	[self.navigationController popViewControllerAnimated:YES];
//	[self dismissViewControllerAnimated:YES completion:NULL];
    
}

-(NSNumber*)convertMonthToNumber:(NSString*)month
{
    NSArray *strings = [month componentsSeparatedByString:@" "];

    NSString * str =strings[0];
    
    NSDictionary* month2number = @{@"jan":@1 ,@"feb":@2 ,@"mar":@3 ,@"apr":@4 ,@"may":@5 ,@"jun":@6 ,@"jul":@7 ,@"aug":@8
                                   ,@"sep":@9 ,@"oct":@10 ,@"nov":@11, @"dec":@12};
    NSNumber * monthNum = month2number[[strings[1] lowercaseString]];
    
    int year = 0;
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([_shelfYear rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        // newString consists only of the digits 0 through 9
        year = [_shelfYear intValue] *10000;
        
    }
    
    int datenum = year + ([monthNum intValue]*100) + [str intValue];
        NSLog(@"date %d %d %d :%d",year,([monthNum intValue]*100),[str intValue],datenum);
    
    NSLog(@"num %d",[[NSNumber numberWithInt:datenum] intValue]);
    return [NSNumber numberWithInt:datenum];
}

-(NSString*)convertNumberToMonth:(NSNumber*)number
{
    NSString * str = [number stringValue];
    NSLog(@"str %@",str);
    NSInteger month = [[str substringWithRange:NSMakeRange(0,1)] intValue];
    NSInteger day = [[str substringWithRange:NSMakeRange(2,3)] intValue];
    
    NSDictionary* num2month = @{@"01":@"Jan",@"02":@"Feb" ,@"03":@"Mar" ,@"04":@"Apr" ,@"05":@"May" ,@"06":@"Jun" ,@"07":@"Jul" ,@"08":@"Aug"
                                ,@"09":@"Sep" ,@"10":@"Oct" ,@"11":@"Nov", @"12":@"Dec"};
    
    return [NSString stringWithFormat:@"%d %@",day,[num2month valueForKey:[str substringWithRange:NSMakeRange(4,2)]]];
}

- (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
@end
