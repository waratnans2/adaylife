//
//  MyDownloadViewController.h
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/22/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyDownloadViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *calendatContentView;
@property (weak, nonatomic) IBOutlet UIScrollView *calendarScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *shelfScroll;
@property (weak, nonatomic) IBOutlet UIView *shelfContentview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shelfViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *moreImageView;
@property (strong, nonatomic) NSMutableArray * yearsArray;


@end
