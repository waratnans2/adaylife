//
//  MyDownloadViewController.m
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/22/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//

#import "MyDownloadViewController.h"
#import "DBManager.h"
#import "Book.h"
#import "BookCover.h"

@interface MyDownloadViewController ()
{
    NSArray *originX;
    NSString * currentYear;
    NSMutableArray * bookArray;
    NSMutableArray * childViewControllerArray;
    
}
@property (nonatomic,retain) DBManager * dbMgr;
@property (nonatomic, strong) UISegmentedControl *mySegmentedControl;

@end

@implementation MyDownloadViewController
@synthesize yearsArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(DBManager*)dbMgr
{
    if(_dbMgr == nil){
        _dbMgr = [[DBManager alloc]init];
    }
    return _dbMgr;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    bookArray = [self.dbMgr fetchBook:nil];
    
    NSLog(@"BOOK ARRAY %d",[bookArray count]);
    currentYear = @"ALL";
    childViewControllerArray = [[NSMutableArray alloc]init];
    [self createYearScroll];
    [self setButtonTo:currentYear];
    [self adjustBook];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark book shelf
- (void)adjustBook
{
    
    float coverWidth,coverHeight;
    float spaceMagin, spaceHeight;
    float shelfBar,shelfBarWidth,shelfBarHeight;
    int nRow,nPerPage;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        originX = [[NSArray alloc] initWithObjects:@"24", @"166", @"511", nil]; // 168
        coverWidth = 130;
        coverHeight = 230;
        spaceHeight = 240;
        spaceMagin = 15;
        nRow = 2;
        nPerPage = 2;
        shelfBar = 144;
        shelfBarHeight = 32;
        shelfBarWidth = 286;
    }
    else{
        originX = [[NSArray alloc] initWithObjects:@"34", @"272", @"511", nil];
        coverWidth = 207;
        coverHeight = 347;
        spaceHeight = 340;
        spaceMagin = 40;
        nRow = 3;
        nPerPage = 6;
        shelfBar = 199;
        shelfBarHeight = 64;
        shelfBarWidth = 734;
    }

    if ([bookArray count] > 0) {
        for (int i=0; i<[bookArray count]; i++)
        {
            //            NSMutableDictionary *issueIndex = [[NSMutableDictionary alloc] initWithDictionary:[self.issueCatalog objectAtIndex:i]];
            //            NSLog(@"ISSUE: %@",[issueIndex objectForKey:@"issue"]);
            //
            //            BookCover *tempCoverView = (LeatherCover*)[scrollView viewWithTag:3000+i];
            //
            //            if (tempCoverView) {
            //                NSLog(@"HAVE");
            //            }
            //            else
            //            {
            
            BookCover *coverView = [[BookCover alloc] init];
            coverView.libraryPage = YES;
            coverView.bookObj = bookArray[i];
//            coverView.bookData = bookArray[i];
//            coverView.shelfYear = currentYear;
            //                coverView.delegate = self;
            //                NSLog(@"Type: %@",type);
            //                coverView.type = type;
            //                coverView.type = @"Booklet";

            coverView.view.tag = 3000+i;
            coverView.view.frame = CGRectMake([[originX objectAtIndex:i%nRow] floatValue], spaceMagin+(spaceHeight* (int)(i/nRow)), coverWidth, coverHeight);
            //                coverView.detailDict = issueIndex;
            //                coverView.alreadyBuy = NO;
            //                coverView.loading = NO;
            if(i%nRow==0){
//                UIImageView * shelfImage = [[UIImageView alloc]initWithFrame:CGRectMake(17,coverView.view.frame.origin.y + shelfBar, shelfBarWidth, shelfBarHeight)];
//                [shelfImage setImage:[UIImage imageNamed:@"shelf ipad-1"]];
//                
//                [_shelfContentview addSubview:shelfImage];
                CGRect frame = CGRectMake(17,coverView.view.frame.origin.y + shelfBar, shelfBarWidth, 1);
                UIView * shelfBar = [[UIView alloc]initWithFrame:frame];
                [shelfBar setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:0.8]];
                [_shelfContentview addSubview:shelfBar];
                
            }
            
            [_shelfContentview addSubview:coverView.view];
            
            [self addChildViewController:coverView];
            [coverView didMoveToParentViewController:self];
            
            [childViewControllerArray addObject:coverView];
            
        }
        
        int row = ([bookArray count]%nRow == 0) ? 0 : 1;
        _shelfViewHeight.constant = ((int)[bookArray count]/nRow+ row) * spaceHeight + 50;
        [_shelfScroll setContentOffset:CGPointZero];
        
    }
    
    if([bookArray count] >6){[_moreImageView setHidden:NO];}else{[_moreImageView setHidden:YES];}
}

- (void)cleanView
{
    //    for (UIView *view in [_shelfContentview subviews]) {
    //        [view removeFromSuperview];
    //    }
    
    for (BookCover * controller in childViewControllerArray) {
        
        [controller willMoveToParentViewController:nil];
        [controller.view removeFromSuperview];
        [controller removeFromParentViewController];
        
    }
    [childViewControllerArray removeAllObjects];
    
    for (UIView *view in [_shelfContentview subviews]) {
        [view removeFromSuperview];
    }
    
}



#pragma mark calendar scroll
-(void)createYearScroll
{
    
    [yearsArray insertObject:@"ALL" atIndex:0];
    
    
    CGRect myFrame;
    UIFont *font;
    NSDictionary *attributes;
    
    
    //frame for the segemented button
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        myFrame = CGRectMake(5.0f, 0.0f, 310.0f, 29.0f);
        font = [UIFont boldSystemFontOfSize:14.0f];
        attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
        
    }else{
        myFrame = CGRectMake(10.0f, 0.0f, 748.0f, 50.0f);
        font = [UIFont boldSystemFontOfSize:18.0f];
        attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
        
    }
    
    //create an intialize our segmented control
    self.mySegmentedControl = [[UISegmentedControl alloc] initWithItems:yearsArray];
    
    //set the size and placement
    self.mySegmentedControl.frame = myFrame;
    [self.mySegmentedControl setTitleTextAttributes:attributes
                                           forState:UIControlStateNormal];
    //default the selection to second item
    [self.mySegmentedControl setSelectedSegmentIndex:0];
    [self.mySegmentedControl setTintColor:[UIColor colorWithRed:0.99 green:0.76 blue:0.23 alpha:1]];
    
    //    //attach target action for if the selection is changed by the user
    [self.mySegmentedControl addTarget:self
                                action:@selector(touchYear:)
                      forControlEvents:UIControlEventValueChanged];
    
    //add the control to the view
    [self.calendatContentView addSubview:self.mySegmentedControl];

}

- (void) showMoreButton:(BOOL)show
{
    if (show) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        _moreImageView.alpha = 1.0;
        [UIView commitAnimations];
    }
    else{
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        _moreImageView.alpha = 0.0;
        [UIView commitAnimations];
        
    }
}

#pragma mark -
- (void)checkPage
{
    CGFloat pageHeight = _shelfScroll.frame.size.height;
    CGFloat contentHeight = _shelfScroll.contentSize.height;
    
    int page = floor((_shelfScroll.contentOffset.y - pageHeight / 2) / pageHeight)+2;
    
    if ( (contentHeight - (pageHeight*page)) > pageHeight){
//        NSLog(@"MORE");
        [self showMoreButton:YES];
    }
    else{
//        NSLog(@"NONE");
        [self showMoreButton:NO];
    }
}

#pragma mark - UIScrollView Delegate

- (void) scrollViewDidScroll:(UIScrollView *)scroll
{
    [self checkPage];
}

#pragma mark calendar button
- (void)touchYear:(UISegmentedControl *)paramSender
{
    NSInteger selectedIndex = [paramSender selectedSegmentIndex];
    currentYear = [paramSender titleForSegmentAtIndex:selectedIndex];
    
    [self setButtonTo:currentYear];
    [self cleanView];
    
    NSString *condition;
    if ([currentYear isEqualToString:@"ALL"]) {
        condition = [NSString stringWithFormat:@""];
    }else{
        condition = [NSString stringWithFormat:@"year = '%@'",currentYear];
    }
    
    bookArray = [self.dbMgr fetchBook:condition];
    [self adjustBook];
    
    condition = nil;


}

- (void)setButtonTo:(NSString*)title
{
    UIButton *selectButton = [[UIButton alloc] init];
    for (int i=0; i<[yearsArray count]; i++)
    {
        for (UIView *view in _calendatContentView.subviews) {
            if ([view isKindOfClass:[UIButton class]])
            {
                UIButton *button = (UIButton*)view;
                [button setTitleColor:[UIColor colorWithRed:81.0/255.0 green:72/255.0 blue:67.0/255.0 alpha:1] forState:UIControlStateNormal];
                
                if ([button.titleLabel.text isEqualToString:title]) {
                    selectButton = button;
                }
            }
        }
    }
    
    [selectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

-(IBAction)closeView:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
-(IBAction)openFanPage:(id)sender
{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile?id=267847432346"]])
        NSLog(@"%@%@",@"Failed to open url:",@"fb://profile?id=267847432346");
}
@end
