//
//  ApiUtils.h
//  EMagazine
//
//  Created by Waratnan Suriyasorn on 4/2/2557 BE.
//  Copyright (c) 2557 Waratnan Suriyasorn. All rights reserved.
//

#define API_REQUEST_BOOKSHELF_BY_YEAR 1
#define API_REQUEST_BOOKDETAIL 2
#define API_REQUEST_SHELF_YEAR 3

#import <Foundation/Foundation.h>
static NSString* host =@"http://aday.blob.core.windows.net/bulletin";//@"http://siamsquared.com/iphone/bulletin";

@protocol ApiUtilsDelegate <NSObject>
@required
- (void) processCompleted:(id)data api:(int)api success:(BOOL)success;
@end

@interface ApiUtils : NSObject <NSXMLParserDelegate>

@property (nonatomic, assign) id <ApiUtilsDelegate> delegate;


-(void)openBrowser:(NSString*)url_string;
-(NSArray*)fetchAllPDFInDir;

-(void)requestShelfYear;
-(void)requestBookShelfByYear:(NSString *)year;
-(void)requestBookDetail:(NSString *)bookDir year:(NSString*)year;
-(void)nextOperation;
-(void)downloadTempFile:(NSString *)dirName year:(NSString*)year fileName:(NSString*)fileName index:(int)index        notificationName:(NSString*)noti book:(id)book;
-(void)downloadFile:(NSString *)dirName year:(NSString*)year fileName:(NSString*)fileName index:(int)index
   notificationName:(NSString*)noti book:(id)book;

@end
