//
//  ApiUtils.m
//  EMagazine
//
//  Created by Waratnan Suriyasorn on 4/2/2557 BE.
//  Copyright (c) 2557 Waratnan Suriyasorn. All rights reserved.
//

/*  issue
 headid
 title
 link
 bookdir
 thumbnail
 cover
 productID
 pubDate
 price
 description
 previewURL
 */
/*
 filename
 tempfile
 totalpage
 gantrackerid
 readerversion
 verticalscroll
 */
#import "ApiUtils.h"
#import "UIProgressView+AFNetworking.h"
#import "AFNetworking.h"
#import "XMLReader.h"
//#import "AFHTTPRequestOperation.h"
//#import "AFURLSessionManager.h"


@implementation ApiUtils
{
    int API_METHOD;
    NSMutableDictionary *currentDictionary;   // current section being parsed
    NSString *currentElementName;
    NSMutableString *outstring;
    NSMutableArray *dataArray;  // completed parsed xml response
    NSOperationQueue* aQueue1;
    NSOperationQueue* aQueue2;
    NSOperationQueue* aQueue3;
    int currentOperation;
}

- (id)init {
    self = [super init];
    if (self) {
        currentOperation = 1;
        aQueue1 =[[NSOperationQueue alloc] init];
        [aQueue1 setMaxConcurrentOperationCount:1];
        aQueue2 =[[NSOperationQueue alloc] init];
        [aQueue2 setMaxConcurrentOperationCount:1];
        aQueue3 =[[NSOperationQueue alloc] init];
        [aQueue3 setMaxConcurrentOperationCount:1];
    }
    return self;
}
-(void)nextOperation
{
    currentOperation = (currentOperation>=3) ? 1 : currentOperation+1;
    NSLog(@"NEXT OPER %d",currentOperation);
}
-(NSOperationQueue*)getCurrentOertation
{
    if(currentOperation == 1)
    {
     return aQueue1;
    }
    else if(currentOperation == 2)
    {
     return aQueue2;
    }else{
      return aQueue3;
    }
}

-(void)requestShelfYear
{
    API_METHOD = API_REQUEST_SHELF_YEAR;
    NSString * url_string = [host stringByAppendingPathComponent:@"issue_year.json"];
    
    NSURL *URL = [NSURL URLWithString:url_string];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    op.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary * dic = [responseObject valueForKey:@"year"];
//        NSLog(@"JSON: %@", dic);
        
        [self.delegate processCompleted:dic api:API_METHOD success:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
       
    }];
    
    [op start];
    
}

-(void)errorRequest
{
    if(API_METHOD == API_REQUEST_BOOKDETAIL){
        [self.delegate processCompleted:nil api:API_METHOD success:NO];
    }else if (API_METHOD == API_REQUEST_BOOKSHELF_BY_YEAR){
        [self.delegate processCompleted:nil api:API_METHOD success:NO];
    }
}
-(void)requestBookShelfByYear:(NSString *)year
{
    API_METHOD = API_REQUEST_BOOKSHELF_BY_YEAR;
    NSString * url_string =  [host stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_Issue.xml",year]];
    [self requestUrl:url_string];
//    [self downloadXMLFile:url_string];
}

-(void)requestBookDetail:(NSString *)bookDir year:(NSString*)year
{
    API_METHOD = API_REQUEST_BOOKDETAIL;

//    NSString * url_string =  [host stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@.xml",year,bookDir,bookDir]];
    NSString * url_string =  [host stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@.xml",bookDir,bookDir]];
    [self requestUrl:url_string];
    
}

-(void)downloadXMLFile:(NSString*)url_String
{
    NSURL * url  =[NSURL URLWithString:url_String];
    NSLog(@"downloadXMLFile:%@",url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    
    NSString *path = [self getDocumentDirectoryPathString];
  
    NSString *destinationPath = [path stringByAppendingPathComponent:[url_String lastPathComponent]];
    
//    [self checkIfDirectoryAlreadyExists:[destinationPath stringByDeletingLastPathComponent]];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:destinationPath append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //parse
        NSData *data = [NSData dataWithContentsOfFile:destinationPath];

        NSXMLParser *XMLParser = [[NSXMLParser alloc] initWithData:data];
        [XMLParser setShouldProcessNamespaces:YES];
        
        // Leave these commented for now (you first need to add the delegate methods)
        XMLParser.delegate = self;
        [XMLParser parse];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self errorRequest];
    }];
    
    [operation start];
    

}
-(void)requestUrl:(NSString*)url_string
{
    NSURL *url = [NSURL URLWithString:url_string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSLog(@"urk %@",url);
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFXMLParserResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSXMLParser *XMLParser = (NSXMLParser *)responseObject;
        [XMLParser setShouldProcessNamespaces:YES];
        
        // Leave these commented for now (you first need to add the delegate methods)
        XMLParser.delegate = self;
        [XMLParser parse];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[ERROR API:%d]url:%@",API_METHOD,url_string);
    }];
    
    [operation start];
    
}


#pragma mark xml pareser
- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    NSLog(@"start doc");
    if(API_METHOD == API_REQUEST_BOOKDETAIL){
        currentDictionary = [NSMutableDictionary dictionary];
    }else if (API_METHOD == API_REQUEST_BOOKSHELF_BY_YEAR){
        dataArray = [[NSMutableArray alloc]init];
    }
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    
    currentElementName = qName;
//    NSLog(@"elementName:%@",elementName);
    
    if (API_METHOD == API_REQUEST_BOOKSHELF_BY_YEAR) {
        
    
        if([qName isEqualToString:@"issue"])
        {
            currentDictionary = [NSMutableDictionary dictionary];
        }
        
//    }else if(API_METHOD == API_REQUEST_BOOKDETAIL){
//        
//        if([qName isEqualToString:@"filename"] ||
//           [qName isEqualToString:@"tempfile"] ||
//           [qName isEqualToString:@"totalpage"] ||
//           [qName isEqualToString:@"gantrackerid"]  ||
//           [qName isEqualToString:@"readerversion"]   ||
//           [qName isEqualToString:@"verticalscroll"])
//        {
//            currentDictionary = [NSMutableDictionary dictionary];
//        }
    }
    outstring = [NSMutableString string];
    

}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (!currentElementName)
        return;

//    NSLog(@"FOUND :%@",string);
    [outstring appendFormat:@"%@", string];

    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
//    NSLog(@"END ELEMENT :%@ qName:%@",elementName,qName);
    
    if (API_METHOD == API_REQUEST_BOOKSHELF_BY_YEAR) {

        if([qName isEqualToString:@"issue"] ||
           [qName isEqualToString:@"headid"] ||
           [qName isEqualToString:@"title"] ||
           [qName isEqualToString:@"link"]  ||
           [qName isEqualToString:@"bookdir"]   ||
           [qName isEqualToString:@"thumbnail"] ||
           [qName isEqualToString:@"cover"] ||
           [qName isEqualToString:@"productID"] ||
           [qName isEqualToString:@"pubDate"]   ||
           [qName isEqualToString:@"price"] ||
           [qName isEqualToString:@"description"]   ||
           [qName isEqualToString:@"previewURL"])
        {
            [currentDictionary setValue:outstring forKey:qName];
//            NSLog(@"OUT STRING AFTER ADD %@",outstring);

        }

        if([qName isEqualToString:@"item"]){
            [dataArray addObject:currentDictionary];
            currentDictionary=nil;
        }
    }else if(API_METHOD == API_REQUEST_BOOKDETAIL){
        
        if([qName isEqualToString:@"filename"] ||
           [qName isEqualToString:@"tempfile"] ||
           [qName isEqualToString:@"totalpage"] ||
           [qName isEqualToString:@"gantrackerid"]  ||
           [qName isEqualToString:@"readerversion"]   ||
           [qName isEqualToString:@"verticalscroll"])
        {
            [currentDictionary setValue:outstring forKey:qName];
        }
    }
    
    outstring = nil;
    currentElementName = nil;
}

- (void) parserDidEndDocument:(NSXMLParser *)parser
{
//    NSLog(@"end doc");
    
    if(API_METHOD == API_REQUEST_BOOKDETAIL){
        [self.delegate processCompleted:currentDictionary api:API_METHOD success:YES];
        
    }else if (API_METHOD == API_REQUEST_BOOKSHELF_BY_YEAR){
        [self.delegate processCompleted:dataArray api:API_METHOD success:YES];
        dataArray = nil;
    }
}

#pragma mark -
-(NSURL*)getDocumentDirectoryPath
{
    NSURL *documentsDirectoryPath = [NSURL fileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]];
    
    return documentsDirectoryPath;
}

-(NSString*)getDocumentDirectoryPathString
{
    NSString *documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
 
    return documentsDirectoryPath;
}


#pragma mark download
-(void)openBrowser:(NSString *)url_string
{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:url_string]])
        NSLog(@"%@%@",@"Failed to open url:",[url_string description]);
}

-(void)checkIfDirectoryAlreadyExists:(NSString *)path
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    BOOL isDir;
    BOOL exists = [fileManager fileExistsAtPath:path isDirectory:&isDir];
    
    if (!exists) {
        /* file exists */
        [self createDirectory:path];
    }
    
    if (isDir) {
        /* file is a directory */
        NSLog(@"exists");
    }
}

-(void)createDirectory:(NSString *)directoryName
{
    NSString *filePathAndDirectory = directoryName;
    NSError *error;
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePathAndDirectory
                                   withIntermediateDirectories:NO
                                                    attributes:nil
                                                         error:&error])
    {
        NSLog(@"Create directory error: %@", error);
    }

}

-(NSArray*)fetchAllPDFInDir
{
    
    NSURL *DirURL = [[self getDocumentDirectoryPath] URLByAppendingPathComponent:@"PDF_Download"];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray * dirContents = [fm contentsOfDirectoryAtURL:DirURL
      includingPropertiesForKeys:@[]
                         options:NSDirectoryEnumerationSkipsHiddenFiles
                           error:nil];
    NSPredicate * fltr = [NSPredicate predicateWithFormat:@"pathExtension='pdf'"];
    NSArray * onlyPDF = [dirContents filteredArrayUsingPredicate:fltr];
//    NSLog(@"%@",onlyPDF);
    return  onlyPDF;
}

-(void)downloadFile:(NSString *)dirName year:(NSString*)year fileName:(NSString*)fileName index:(int)index
       notificationName:(NSString*)noti book:(id)book
{
    
    NSURL * url  =[NSURL URLWithString:[[host stringByAppendingString:dirName] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.pdf",index,fileName]]];

    NSLog(@"%@",url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    NSString *path = [self getDocumentDirectoryPathString];
    NSString *destinationPath = [[path stringByAppendingPathComponent:dirName] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.pdf",index,fileName]];
    [self checkIfDirectoryAlreadyExists:[destinationPath stringByDeletingLastPathComponent]];

    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:destinationPath append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Successfully downloaded file %@", [destinationPath lastPathComponent]);
        
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:index],@"pageIndex",
                                  book,@"Book",nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:noti object:nil userInfo:userInfo];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"finishDownloadPDFFileForMainNotification" object:nil userInfo:userInfo];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
//    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long  totalBytesRead, long long totalBytesExpectedToRead) {
//        
//    }];
//    [operation start];
    
    [[self getCurrentOertation] addOperation:operation];

//    [[NSOperationQueue mainQueue] addOperation:operation];
    
}

-(void)downloadTempFile:(NSString *)dirName year:(NSString*)year fileName:(NSString*)fileName index:(int)index
       notificationName:(NSString*)noti book:(id)book
{

    NSURL * url  =[NSURL URLWithString:[[host stringByAppendingString:dirName] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.png",index,fileName]]];
    
//    NSLog(@"%@",url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    
    NSString *path = [self getDocumentDirectoryPathString];
    NSString *destinationPath = [[path stringByAppendingPathComponent:dirName] stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.png",index,fileName]];
    
    [self checkIfDirectoryAlreadyExists:[destinationPath stringByDeletingLastPathComponent]];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:destinationPath append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
//        NSLog(@"Successfully downloaded file to %@", destinationPath);
//        [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:index] forKey:@"pageIndex"];
        
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:index],@"pageIndex",
                                                    book,@"Book",nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:noti object:nil userInfo:userInfo];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"finishDownloadTempFileForMainNotification" object:nil userInfo:userInfo];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long  totalBytesRead, long long totalBytesExpectedToRead) {
        
        //      NSLog(@"%@ Download = %f ",name, (float)totalBytesRead / totalBytesExpectedToRead);
        //      float progress =(float)totalBytesRead / totalBytesExpectedToRead;
        //        NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
        //        [userInfo setObject:name forKey:@"fileName"];
        //        [userInfo setObject:[NSNumber numberWithInteger:totalBytesRead] forKey:@"totalBytesRead"];
        //        [userInfo setObject:[NSNumber numberWithInteger:totalBytesExpectedToRead] forKey:@"totalBytesExpectedToRead"];
        ////        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:totalBytesRead forKey:@"progress"];
        //        [[NSNotificationCenter defaultCenter] postNotificationName: @"TestNotification" object:nil userInfo:userInfo];
    }];
    
//    [progress setProgressWithDownloadProgressOfOperation:operation animated:YES];
//    [operation start];

//    [[NSOperationQueue mainQueue] addOperation:operation];
    [[self getCurrentOertation] addOperation:operation];
  
}
@end