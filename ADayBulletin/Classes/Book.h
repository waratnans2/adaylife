//
//  Book.h
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/22/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Book : NSManagedObject

@property (nonatomic, retain) NSData * bookmarks;
@property (nonatomic, retain) NSString * bookName;
@property (nonatomic, retain) NSData * cover;
@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSString * filePath;
@property (nonatomic, retain) NSString * pageCount;
@property (nonatomic, retain) NSString * pageDownloaded;
@property (nonatomic, retain) NSString * pageNumber;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * tempFile;
@property (nonatomic, retain) NSData * thumbNail;
@property (nonatomic, retain) NSString * year;
@property (nonatomic, retain) NSNumber * pubDate;

@end
