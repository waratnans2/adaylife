//
//  Book.m
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/22/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//

#import "Book.h"


@implementation Book

@dynamic bookmarks;
@dynamic bookName;
@dynamic cover;
@dynamic fileName;
@dynamic filePath;
@dynamic pageCount;
@dynamic pageDownloaded;
@dynamic pageNumber;
@dynamic password;
@dynamic state;
@dynamic tempFile;
@dynamic thumbNail;
@dynamic year;
@dynamic pubDate;

@end
