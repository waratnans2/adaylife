//
//  DBManager.h
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/21/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"
#import "AppDelegate.h"

@interface DBManager : NSObject
{
    NSMutableArray* memArrayObj;

}
@property (nonatomic, retain) NSMutableArray* memArrayObj;

+(DBManager*) getDBManager;

// -------------------------------
-(NSMutableArray*)fetchBook:(NSString*)cond;    // select
-(Book*)getSchemaBook;                // get for new
-(BOOL)insertDBBook:(Book*)_book;   // save new
-(BOOL)updateDBBook:(Book*)_book;   // update
-(BOOL)deleteDBBook:(Book*)_book;   // delete

@end
