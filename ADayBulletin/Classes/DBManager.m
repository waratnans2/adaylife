//
//  DBManager.m
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/21/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//

#import "DBManager.h"

static DBManager* _dbManager = nil;

@implementation DBManager

@synthesize memArrayObj;

-(id) init{
    
    if (self = [super init])
    {
        // Initialization code here
        memArrayObj = [[NSMutableArray alloc] init];
    }
    return self;
}

+(DBManager*) getDBManager{
    @synchronized(self){
        if(_dbManager == nil){
            _dbManager = [[self alloc] init];
        }
    }
    return _dbManager;
}

#pragma mark Book management

-(NSMutableArray *)fetchBook:(NSString *)cond
{
    NSMutableArray *ar =nil;
    NSManagedObjectContext *context = [self getManageObjectContext];
    NSEntityDescription *en = [NSEntityDescription entityForName:@"Book" inManagedObjectContext:context];
    
    // fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:en];
    
    // condition
    if( ![cond isEqualToString:@""]){
        // cond --> sql syntax in where
        NSPredicate *predrequesticate = [NSPredicate predicateWithFormat:cond];
        [request setPredicate:predrequesticate];
    }
    
//     sorting
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pubDate" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        [request setSortDescriptors:sortDescriptors];
    
    //array
    NSError* err;
    NSArray* objects = [context executeFetchRequest:request error:&err];
    
    NSLog(@"count = %d", [objects count]);
    if([objects count] > 0){
        ar = [[NSMutableArray alloc] initWithCapacity: [objects count]];
        
        for(int i=0; i< [objects count]; i++){
            Book *obj = [objects objectAtIndex:i];
            [ar addObject:obj];
        }
    }
    
    //    [request release];
    //[memArrayObj addObject:ar];
    return ar;
}

-(Book*)getSchemaBook
{
    NSManagedObjectContext *context = [self getManageObjectContext];
    Book *newObj = [NSEntityDescription insertNewObjectForEntityForName:@"Book" inManagedObjectContext:context];
    return newObj;
}

-(BOOL)insertDBBook:(Book *)_book
{
    //    NSManagedObjectContext *context = [self getManageObjectContext];
    //    Member *newMember = [NSEntityDescription insertNewObjectForEntityForName:@"Member" inManagedObjectContext:context];
    //    newMember.exp_date_app = _member.exp_date_app;
    //    newMember.exp_date_member = _member.exp_date_member;
    //    newMember.msisdn = _member.msisdn;
    //    newMember.otp_member_flag = _member.otp_member_flag;
    //    newMember.package_type = _member.package_type;
    
    NSError* err;
    if(![[self getManageObjectContext] save:&err]){
        NSLog(@"error = %@ , %@", err, [err userInfo]);
        return false;
    }else{
        NSLog(@"Book save complete");
        return true;
    }
    return false;
}

-(BOOL)updateDBBook:(Book *)_book
{
    
    NSManagedObjectContext *context = [self getManageObjectContext];
    NSEntityDescription *en = [NSEntityDescription entityForName:@"Book" inManagedObjectContext:context];
    
    // fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:en];
    //[en release];
    
    // condition
    NSPredicate *predrequesticate = [NSPredicate predicateWithFormat:@"bookName = %@",_book.bookName];
    [request setPredicate:predrequesticate];
    
    //array
    NSError* err;
    NSArray* objects = [context executeFetchRequest:request error:&err];
    
    if(objects && [objects count]>0){
        //update data
        Book *obj = [objects objectAtIndex:0];
        obj.bookName = _book.bookName;
        obj.filePath = _book.filePath;
        obj.tempFile = _book.tempFile;
        obj.pageDownloaded = _book.pageDownloaded;
        obj.pageCount = _book.pageCount;
        obj.pageNumber = _book.pageNumber;
        obj.password = _book.password;
        obj.fileName = _book.fileName;
        obj.bookmarks = _book.bookmarks;
        obj.thumbNail = _book.thumbNail;
        obj.cover = _book.cover;
        obj.state = _book.state;
        obj.year = _book.year;
        obj.pubDate = _book.pubDate;
        
    }
    
    
    //    [request release];
    
    
    NSError* er;
    if(![[self getManageObjectContext] save:&er]){
        NSLog(@"error = %@ , %@", er, [er userInfo]);
        return false;
    }else{
        NSLog(@"Book update complete");
        return true;
    }
    return false;
}


-(BOOL)deleteDBBook:(Book *)_book
{
    
    NSManagedObjectContext *context = [self getManageObjectContext];
    NSEntityDescription *en = [NSEntityDescription entityForName:@"Book" inManagedObjectContext:context];
    
    // fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:en];
    //[en release];
    
    // condition
    NSPredicate *predrequesticate = [NSPredicate predicateWithFormat:@"bookName = %@",_book.bookName];
    [request setPredicate:predrequesticate];
    
    //array
    NSError* err;
    NSArray* objects = [context executeFetchRequest:request error:&err];
    
    if(objects && [objects count]>0){
        //update data
        Book *obj = [objects objectAtIndex:0];
        [context deleteObject:obj];
    }
    
    NSError* er;
    [[self getManageObjectContext] deleteObject:_book];
    if(![[self getManageObjectContext] save:&er]){
        NSLog(@"error = %@ , %@", er, [er userInfo]);
        return false;
    }else{
        NSLog(@"Book delete complete");
        return true;
    }
    return true;
}

#pragma mark util
-(NSManagedObjectContext*)getManageObjectContext{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    return context;
}
@end
