//
//  CoverShootViewConroller.h
//  aday
//
//  Created by Batt on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "OverlayViewController.h"
@interface CoverShootViewConroller : UIViewController
<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,ShareCoverShootViewControllerDelegate>
{
    IBOutlet UIView *camaraView;
    IBOutlet UIView *overlayView;
    IBOutlet UIView *toolView;
    IBOutlet UIImageView *resultImageView;
    
    IBOutlet UIButton *imageLibButton;
    IBOutlet UIButton *shutterButton;
    IBOutlet UIButton *homeButton;
    
    IBOutlet UIButton *backButton;
    IBOutlet UIButton *saveButton;
    IBOutlet UIButton *previewButton;
        
    OverlayViewController *overlayVC;
    OverlayViewController *editOverlay;
    OverlayViewController *overlayTemp;
    UIImagePickerController *cameraPicker;

    UITapGestureRecognizer *touchImage;
    BOOL isEditPage;
    BOOL isFromSpecialMenu;
    NSMutableDictionary *specialMenuDictionary;
}

-(id)initCoverShootWithSpecialMenu;

- (IBAction)dismissView:(id)sender;
- (IBAction)saveToLibrary:(id)sender;
- (IBAction)previewImage:(id)sender;

- (IBAction)chooseImageFromImageLib:(id)sender;
- (IBAction)takePhoto:(id)sender;
- (IBAction)backToCameraView:(id)sender;

@property (nonatomic)BOOL isEditPage;
@property (nonatomic) UIPopoverController* popoverController;
@end
