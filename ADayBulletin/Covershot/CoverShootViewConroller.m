//
//  CoverShootViewConroller.m
//  aday
//
//  Created by Batt on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CoverShootViewConroller.h"
#import "AppDelegate.h"

@interface CoverShootViewConroller() 

- (UIImage *) imageWithView:(UIView *)view;
- (void)setPageEdit:(BOOL)boo;
- (void)choosePhoto;
- (UIImage*)scaleImage:(UIImage*)anImage withEditingInfo:(NSDictionary*)editInfo;
- (UIImage *)cropImage:(UIImage *)image to:(CGRect)cropRect andScaleTo:(CGSize)size;
- (UIImage*)scaleImage:(UIImage*)image ToSize:(CGSize)newSize;

- (void)touchResultImage;
- (void)setOutputImage:(UIImage*)img;
- (UIImage*)addOverlayToBaseImage:(UIImage*)baseImage;
- (UIImage*)rotateToPortrait:(UIImage*)src;

@end

@implementation CoverShootViewConroller
@synthesize isEditPage;
@synthesize popoverController;
- (id)init
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self = [super initWithNibName:@"CoverShootViewConroller" bundle:nil];
        overlayVC = [[OverlayViewController alloc] init];
        overlayVC.view.frame = CGRectMake(0, 0, 768, 930);
    }
    else
    {
        self = [super initWithNibName:@"CoverShootViewConroller_iPhone" bundle:nil];
        overlayVC = [[OverlayViewController alloc] init];
        overlayVC.view.frame = CGRectMake(0, 0, 320, 416);
    }
    
    if (self) {
        isFromSpecialMenu = NO;
    }
    return self;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initCoverShootWithSpecialMenu{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self = [super initWithNibName:@"CoverShootViewConroller" bundle:nil];
        overlayVC = [[OverlayViewController alloc] init];
        overlayVC.view.frame = CGRectMake(0, 0, 768, 930);
    }
    
    else
    {
        self = [super initWithNibName:@"CoverShootViewConroller_iPhone" bundle:nil];
        overlayVC = [[OverlayViewController alloc] init];
        overlayVC.view.frame = CGRectMake(0, 0, 320, 416);
    }
    
    if (self) {
        isFromSpecialMenu = YES;
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//        specialMenuDictionary = appDelegate.specialMenu.specialMenuDictionary;
        [overlayVC setIsSpecialMenu:YES];
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) viewDidAppear:(BOOL)animated {
//	OverlayView *overlay = [[OverlayView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGTH)];

    NSLog(@"viewDidAppear");
    [super viewDidAppear:YES];
    
	// Create a new image picker instance:
	cameraPicker = [[UIImagePickerController alloc] init];
	cameraPicker.delegate = self;
    
	// Set the image picker source:
	cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
	
	// Hide the controls:
	cameraPicker.showsCameraControls = NO;
	cameraPicker.navigationBarHidden = YES;
	
	// Make camera view full screen:
    cameraPicker.wantsFullScreenLayout = YES;
    
    //	picker.cameraViewTransform = CGAffineTransformScale(picker.cameraViewTransform, CAMERA_TRANSFORM_X, CAMERA_TRANSFORM_Y);
    
	
	// Insert the overlay:
    
    if(isFromSpecialMenu == YES){
        overlayTemp = [[OverlayViewController alloc] initOverlayWithSpecialMenu];
        [overlayTemp setIsSpecialMenu:YES];
    }
    else{
        overlayTemp = [[OverlayViewController alloc] init];
    }
	cameraPicker.cameraOverlayView = overlayTemp.view;
	// Show the picker:
    [camaraView addSubview:cameraPicker.view];
//    [overlayTemp release];

    //	[self presentModalViewController:picker animated:YES];	
//	[picker release];
	
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setPageEdit:NO];
    
}

-(void)viewWillAppear:(BOOL)animated{
    if(isFromSpecialMenu == YES){
        [imageLibButton setHidden:YES];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    camaraView = nil;
//    [cameraPicker release];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)touchResultImage
{
    if (toolView.hidden == YES) {
        toolView.hidden = NO;
        overlayVC.enableediting = YES;
        if(isFromSpecialMenu == YES){
            
            NSString *bookno = [specialMenuDictionary objectForKey:@"bookno"];
            [overlayTemp.numberLabel setText:bookno];
            [overlayVC.numberLabel setText:bookno];
            [overlayTemp.changeNumberButton setHidden:YES];
            [overlayVC.changeNumberButton setHidden:YES];
        }
        
        [overlayView removeGestureRecognizer:touchImage];

    }
}


- (void)setPageEdit:(BOOL)boo
{
 
    
    if (boo) {
        backButton.hidden = NO;
        saveButton.hidden = NO;
        previewButton.hidden = NO;
        
        imageLibButton.hidden = YES;
        shutterButton.hidden = YES;
        homeButton.hidden = YES;
        overlayVC.enableediting = YES;
        overlayView.hidden = NO;

        resultImageView.hidden = NO;
        camaraView.hidden = YES;

    }
    else
    {
        imageLibButton.hidden = NO;
        shutterButton.hidden = NO;
        homeButton.hidden = NO;
        
        backButton.hidden = YES;
        saveButton.hidden = YES;
        previewButton.hidden = YES;
        overlayVC.enableediting = NO;
        overlayView.hidden = YES;

        resultImageView.hidden = YES;
        camaraView.hidden = NO;

    }
}

- (void)setOutputImage:(UIImage*)img
{
    
    resultImageView.image = img;

    [self setPageEdit:YES];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
    {
        overlayVC.view.frame = CGRectMake(0, 0, 768, 930);
    }
    else
    {
        NSLog(@"setOutput");        
    }
    overlayVC.view.hidden = NO;
    overlayVC.enableediting = YES;
    [overlayView addSubview:overlayVC.view];
    [overlayTemp.leftSpecialButton setHidden:YES];
    [overlayTemp.rightSpecialButton setHidden:YES];
    
    if(isFromSpecialMenu == YES){
        
        NSString *bookno = [specialMenuDictionary objectForKey:@"bookno"];
        [overlayTemp.numberLabel setText:bookno];
        [overlayVC.numberLabel setText:bookno];
        [overlayTemp.changeNumberButton setHidden:YES];
        [overlayVC.changeNumberButton setHidden:YES];
        [overlayVC.leftSpecialButton setHidden:YES];
        [overlayVC.rightSpecialButton setHidden:YES];
        [overlayTemp.leftSpecialButton setHidden:YES];
        [overlayTemp.rightSpecialButton setHidden:YES];
    }
}

#pragma mark - UIImagePicker
- (void)choosePhoto {
    
    //NSLog(@"%s", __FUNCTION__);
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
//    imagePicker.view.frame = CGRectMake(0, 0, 480, 640);
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        imagePicker.allowsEditing = YES;

        UIPopoverController* aPopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker]; 
        [aPopover setPopoverContentSize:CGSizeMake(320, 570)];
        [aPopover presentPopoverFromRect:imageLibButton.frame inView:toolView permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES]; 
        self.popoverController = aPopover;

    }
    else
    {
        imagePicker.allowsEditing = NO;
        UIImage *image = [[UIImage alloc] init];
        image = [UIImage imageNamed:@"BG_Navigation.png"];
        [imagePicker.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
//        if ([imagePicker.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) 
//        {
//            NSLog(@"SET");
//            UIImage *image = [UIImage imageNamed:@"BG_Navigation.png"];
//            [imagePicker.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
//        }
        [self presentViewController:imagePicker animated:YES completion:nil];
//        [image release];
    }
    
//    [imagePicker release];

//    [imagePicker release];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)editInfo {

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.popoverController dismissPopoverAnimated:YES];
    }
    else{
        [self dismissModalViewControllerAnimated:YES];
    }
    
    
    resultImageView.userInteractionEnabled=YES;
    CGRect imageFrame = resultImageView.frame;
    CGPoint imageCenter = resultImageView.center;
    UIImage *croppedImage = [[UIImage alloc] init];
    
    UIImage *img = [[UIImage alloc] init];
//    img = [editInfo objectForKey:UIImagePickerControllerOriginalImage];
    img = [editInfo objectForKey:UIImagePickerControllerEditedImage];
    if (!img) {
        img = [editInfo objectForKey:UIImagePickerControllerOriginalImage];
    }
    NSMutableDictionary *imageDescriptor = [[NSMutableDictionary alloc] init];
    imageDescriptor = [editInfo mutableCopy];
    
    // CGFloat scaleSize = 400.0f;
    CGFloat scaleSize = 640.0f;

    switch ([picker sourceType]) {
            //done
        case UIImagePickerControllerSourceTypePhotoLibrary:
            NSLog(@"UIImagePickerControllerSourceTypePhotoLibrary");
            CGRect origRect;
            origRect.origin.x = 0;
            origRect.origin.y = 0;
            origRect.size.width = 480;
            origRect.size.height = 640;


            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                croppedImage = img;
            }
            else
            {
                UIImageOrientation originalOrientation = [img imageOrientation];
                if (originalOrientation == UIImageOrientationUp || 
                    originalOrientation == UIImageOrientationDown) 
                {
                    NSLog(@"Up Down");
                    img = [self rotateToPortrait:img];
                    
                }
                croppedImage = [self scaleImage:img ToSize:CGSizeMake(720, 960)];
            }
            [imageDescriptor setObject:croppedImage forKey:@"croppedImage"];

            break;
            
            
        case UIImagePickerControllerSourceTypeCamera:{
            NSLog(@"UIImagePickerControllerSourceTypeCamera");
            NSLog(@"SAVE");
            
            croppedImage = [self scaleImage:img ToSize:CGSizeMake(resultImageView.frame.size.width, resultImageView.frame.size.height)];            
            
/*            if(isFromSpecialMenu == YES){
                resultImageView.contentMode = UIViewContentModeRedraw;
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                NSMutableDictionary *specialMenuDic = appDelegate.specialMenu.specialMenuDictionary;
                NSMutableArray *filterArray = [specialMenuDic objectForKey:@"filtercomponent"];
                
                UIScrollView *specialFilterScrollView = overlayTemp.specialFilterScrollView;
                int currentPageIndex = specialFilterScrollView.contentOffset.x/specialFilterScrollView.frame.size.width;
                NSMutableDictionary *currentFilterInfo = [filterArray objectAtIndex:currentPageIndex];
                UIImageView *currentFilterImageView = [currentFilterInfo objectForKey:@"filterimageview"];
                UIImage *currentFilterImage = currentFilterImageView.image;
                croppedImage = [self addImage:croppedImage ToImage:currentFilterImage WithSize:croppedImage.size];
            }
    */
            [imageDescriptor setObject:croppedImage forKey:@"croppedImage"];

            
//            UIImageOrientation originalOrientation = [[editInfo objectForKey:UIImagePickerControllerOriginalImage] imageOrientation];
//            if (originalOrientation == UIImageOrientationUp || 
//                originalOrientation == UIImageOrientationDown) 
//            {
////                NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
////                UIImage *originalImage = [[UIImage alloc] init];
////                originalImage = [editInfo objectForKey:UIImagePickerControllerOriginalImage];
////                UIImage *rotatedImage = [self rotateToPortrait:originalImage];
////                croppedImage = rotatedImage;
////                [imageDescriptor setObject:croppedImage forKey:@"croppedImage"];
////
////                [pool drain];
//            }
//            else 
//            {
//            }
        }
            break;
            
        case UIImagePickerControllerSourceTypeSavedPhotosAlbum: {
            NSLog(@"UIImagePickerControllerSourceTypeSavedPhotosAlbum");

            UIImageOrientation originalOrientation = [[editInfo objectForKey:UIImagePickerControllerOriginalImage] imageOrientation];
                       
            if (originalOrientation != UIImageOrientationUp) {
//                NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
                CGRect origRect;
                [[editInfo objectForKey:UIImagePickerControllerCropRect] getValue:&origRect];
                UIImage *rotatedImage = img;

                CGFloat scale = scaleSize/640.0f;
                origRect.origin.x *= scale;
                origRect.origin.y *= scale;
                origRect.size.width *= scale;
                origRect.size.height *= scale;
                croppedImage = [self cropImage:rotatedImage to:origRect andScaleTo:CGSizeMake(320, 480)];
                [imageDescriptor setObject:croppedImage forKey:@"croppedImage"];
//                [pool drain];
            }
            else {
                croppedImage = [self scaleImage:img withEditingInfo:editInfo];
                [imageDescriptor setObject:croppedImage forKey:@"croppedImage"];
            }
        }
            break;
        default:
            break;
    
    }
    
    imageFrame.size = croppedImage.size;
    [self setOutputImage:[[imageDescriptor objectForKey:@"croppedImage"] copy]];
    NSLog(@"Result Image W: %f  H: %f",resultImageView.image.size.width ,resultImageView.image.size.height);
    
    camaraView.hidden = YES;
//    [croppedImage release];
//    [imageDescriptor release];

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
//    [picker release];
}

- (UIImage*)addImage:(UIImage*)img ToImage:(UIImage*)img2 WithSize:(CGSize)withsize{
    
    CGSize size = withsize;
    UIGraphicsBeginImageContext(size);
    
    CGPoint pointImg1 = CGPointMake(0,0);
    [img drawAtPoint:pointImg1 ];
    
    CGPoint pointImage2 = CGPointMake(0, 0);
    [img2 drawAtPoint:pointImage2 ];
    
    UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

- (UIImage*)scalingAndCroppingImage:(UIImage *)sourceImage ForSize:(CGSize)targetSize
{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
        {
            scaleFactor = widthFactor; // scale to fit height
        }
        else
        {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
        {
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
        }
    }
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil)
    {
        NSLog(@"could not scale image");
    }
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark - View to Imgae
- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage*)scaleImage:(UIImage*)image ToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage*)scaleImage:(UIImage*)anImage withEditingInfo:(NSDictionary*)editInfo{
    
    UIImage *newImage;
    
    UIImage *originalImage = [editInfo valueForKey:@"UIImagePickerControllerOriginalImage"];
    CGSize originalSize = CGSizeMake(originalImage.size.width, originalImage.size.height);
    CGRect originalFrame;
    originalFrame.origin = CGPointMake(0,0);
    originalFrame.size = originalSize;
    
    CGRect croppingRect = [[editInfo valueForKey:@"UIImagePickerControllerCropRect"] CGRectValue];
    CGSize croppingRectSize = CGSizeMake(croppingRect.size.width, croppingRect.size.height);
    
    CGSize croppedScaledImageSize = anImage.size;
    
    float scaledBarClipHeight = 80;
    
    CGSize scaledImageSize;
    float scale;
    
    if(!CGSizeEqualToSize(croppedScaledImageSize, originalSize)){
        
        scale = croppedScaledImageSize.width/croppingRectSize.width;
        float barClipHeight = scaledBarClipHeight/scale;
        
        croppingRect.origin.y -= barClipHeight;
        croppingRect.size.height += (2*barClipHeight);
        
        if(croppingRect.origin.y<=0){
            croppingRect.size.height += croppingRect.origin.y;
            croppingRect.origin.y=0;
        }
        
        if(croppingRect.size.height > (originalSize.height - croppingRect.origin.y)){
            croppingRect.size.height = (originalSize.height - croppingRect.origin.y);
        }
        
        
        scaledImageSize = croppingRect.size;
        scaledImageSize.width *= scale;
        scaledImageSize.height *= scale;
        

        newImage =  [self cropImage:originalImage to:croppingRect andScaleTo:scaledImageSize];

        
    }else{
        
        newImage = originalImage;
        
    }
    
    
    return newImage;
}

- (UIImage *)cropImage:(UIImage *)image to:(CGRect)cropRect andScaleTo:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGImageRef subImage = CGImageCreateWithImageInRect([image CGImage], cropRect);
    CGRect myRect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    CGContextScaleCTM(context, 1.0f, -1.0f);
    CGContextTranslateCTM(context, 0.0f, -size.height);
    CGContextDrawImage(context, myRect, subImage);
    UIImage* croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRelease(subImage);
    return croppedImage;
}

- (UIImage*)rotateToPortrait:(UIImage*)src
{
    UIImageOrientation orientation = [src imageOrientation];

    UIImage *rotatedImage = [[UIImage alloc] init];
    
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
//    {
        if (orientation == UIImageOrientationRight) {
            NSLog(@"UIImageOrientationRight");

        } else if (orientation == UIImageOrientationLeft) {
            NSLog(@"UIImageOrientationLeft");

        } else if (orientation == UIImageOrientationDown) {
            NSLog(@"UIImageOrientationDown");
            rotatedImage = [[UIImage alloc] initWithCGImage: src.CGImage
                                                      scale: 1.0
                                                orientation: UIImageOrientationRight];
        } else if (orientation == UIImageOrientationUp) {
            NSLog(@"UIImageOrientationUp");
            rotatedImage = [[UIImage alloc] initWithCGImage: src.CGImage
                                                      scale: 1.0
                                                orientation: UIImageOrientationRight];
        }
        
//    }
    return rotatedImage;
}


#pragma mark -

- (UIImage*)addOverlayToBaseImage:(UIImage*)baseImage {
    
    overlayVC.enableediting = NO;
    UIImage *overlayImage = [[UIImage alloc] init];
	overlayImage = [self imageWithView:overlayVC.view];	
//    NSLog(@"W: %f  H:%f",overlayImage.size.width,overlayImage.size.height);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        overlayImage = [self scaleImage:overlayImage ToSize:CGSizeMake(720, 936)];
    }
//    NSLog(@"NEW W: %f  H:%f",overlayImage.size.width,overlayImage.size.height);

	overlayVC.enableediting = YES;
	
	CGPoint topCorner = CGPointMake(0, 0);
	CGSize targetSize;// = CGSizeMake(320, 427);
	CGRect scaledRect = CGRectZero;
	CGFloat scaled;
	CGFloat offsetX;
	
    scaled = baseImage.size.width / baseImage.size.height;
	//lanscape
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
    {
        targetSize = CGSizeMake(768, 1024);

        scaled = 1024 * baseImage.size.width / baseImage.size.height;
        offsetX = (scaled - 768) / -2;
    }
    else
    {
        targetSize = CGSizeMake(720, 960);
        
        scaled = 960 * baseImage.size.width / baseImage.size.height;
        offsetX = (scaled - 720) / -2;
        
    }
    
	
    
	
	
	scaledRect.origin = CGPointMake(0, 0.0);
	//scaledRect.size.width  = scaledX;
	//scaledRect.size.height = scaledX == 320 ? 427:320;
	
	scaledRect.size = targetSize;
	
	UIGraphicsBeginImageContext(targetSize);
	[baseImage drawInRect:scaledRect];	
	[overlayImage drawAtPoint:topCorner];
	UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();	
//	[overlayImage release];
	return result;	
}


#pragma mark - IBAction

- (IBAction)chooseImageFromImageLib:(id)sender
{
    [self choosePhoto];
}

- (IBAction)dismissView:(id)sender
{
    [cameraPicker.view removeFromSuperview];
//    [cameraPicker release];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)backToCameraView:(id)sender
{
    [self setPageEdit:NO];
    if (isFromSpecialMenu == YES) {
        [overlayTemp.leftSpecialButton setHidden:NO];
        [overlayTemp.rightSpecialButton setHidden:NO];
        [imageLibButton setHidden:YES];
    }
}

- (IBAction)saveToLibrary:(id)sender
{
    UIImage *img = [[UIImage alloc] init];
    img = [self addOverlayToBaseImage:resultImageView.image];
    
    ShareCoverShootViewController *shareImageVC = [[ShareCoverShootViewController alloc] initWithImage:img];
    shareImageVC.resultImage = img;
    shareImageVC.delegate = self;
    [self.navigationController  pushViewController:shareImageVC animated:YES];
    
    UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);    
    NSLog(@"Save Done");
}

- (IBAction)previewImage:(id)sender
{
    overlayVC.enableediting = NO;
    toolView.hidden = !toolView.hidden;
    
    touchImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchResultImage)];
	touchImage.numberOfTouchesRequired = 1;
    touchImage.numberOfTapsRequired = 1; 
    touchImage.delegate = self;
    [overlayView addGestureRecognizer:touchImage];

    
    if(isFromSpecialMenu == YES){
        
        NSString *bookno = [specialMenuDictionary objectForKey:@"bookno"];
        [overlayTemp.numberLabel setText:bookno];
        [overlayVC.numberLabel setText:bookno];
        [overlayTemp.changeNumberButton setHidden:YES];
        [overlayVC.changeNumberButton setHidden:YES];
    }
}

- (IBAction)takePhoto:(id)sender
{
    [cameraPicker takePicture];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
//    // Return YES for supported orientations
//    if (interfaceOrientation == UIInterfaceOrientationPortrait ||
//        interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
//        return YES;
//    }
//    else{
//        return NO;
//    }
    return (interfaceOrientation == UIInterfaceOrientationPortrait);

}

@end
