//
//  NumpadViewController.h
//  aday
//
//  Created by Batt on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NumpadViewControllerDelegate

- (void)doneEditNumber:(NSString*)str;

@end

@interface NumpadViewController : UIViewController
{
    NSString *currentStr;
    IBOutlet UILabel *label;
}
@property (nonatomic) int number;
@property (nonatomic, assign) id<NumpadViewControllerDelegate, NSObject> delegate;

- (void)resetLabel;
- (IBAction)toucheButton:(id)sender;
@end
