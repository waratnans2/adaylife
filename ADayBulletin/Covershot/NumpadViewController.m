//
//  NumpadViewController.m
//  aday
//
//  Created by Batt on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "NumpadViewController.h"

@implementation NumpadViewController

@synthesize delegate;
@synthesize number;
static int currentNumber;

- (id)init
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        NSLog(@"iPad");
        self = [super initWithNibName:@"NumpadViewController" bundle:nil];
    }
    else
    {
        NSLog(@"iPhone");
        self = [super initWithNibName:@"NumpadViewController_iPhone" bundle:nil];
    }
    
    if (self) {
        // Custom initialization
    }
    
    return self;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    currentStr = [[NSString alloc] init];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)setNumber:(int)num
{
    currentStr = [NSString stringWithFormat:@"%d",num];
    label.text = currentStr;
}

#pragma mark - IBaction
- (IBAction)toucheButton:(id)sender
{
    UIButton *button = (UIButton*)sender;
    int tag = button.tag;
    
    int selectNumber = tag-90;
    
    if (selectNumber >= 200) 
    {
        self.number = currentNumber;
        [self.delegate doneEditNumber:currentStr];
    }
    else if (selectNumber >= 100)
    {
        currentNumber = 0;
    }
    else
    {
        if (currentNumber < 100) {
            currentNumber = currentNumber*10 + selectNumber;
        }
    }
    self.number = currentNumber;
}

- (void)resetLabel
{
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
