//
//  OverlayViewController.h
//  aday
//
//  Created by Batt on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NumpadViewController.h"
#import "ShareCoverShootViewController.h"

@interface OverlayViewController : UIViewController
<UIGestureRecognizerDelegate, UIScrollViewDelegate,NumpadViewControllerDelegate>
{
    NumpadViewController *numpadVC;
    
    
    IBOutlet UIImageView *adayLogo;
    IBOutlet UIScrollView *colorTabScrollView;
    IBOutlet UIButton *leftButton;
    IBOutlet UIButton *rightButton;
    
    IBOutlet UIButton *changeColorTabButton;
    IBOutlet UIButton *changeNumberButton;
    

    UITapGestureRecognizer *touch;

    
    BOOL editColorTab;
    BOOL enableediting;
    BOOL enableTouchScrollView;
    
    NSMutableDictionary *specialMenuDictionary;
    BOOL isSpecialMenu;
}
@property (nonatomic) BOOL enableediting;
@property (nonatomic) BOOL isSpecialMenu;
@property (nonatomic , strong) IBOutlet UIScrollView *specialFilterScrollView;
@property (nonatomic , assign) int currentSpecialFilterIndex;
@property (nonatomic , strong) IBOutlet UIButton *leftSpecialButton;
@property (nonatomic , strong) IBOutlet UIButton *rightSpecialButton;
@property (nonatomic , strong) IBOutlet UIButton *changeNumberButton;
@property (nonatomic , strong) IBOutlet UILabel *numberLabel;



-(int)getCurrentFilterIndex;
-(id)initOverlayWithSpecialMenu;
- (void)showEditButton:(BOOL)show;

- (IBAction)ChangeColor:(id)sender;
- (IBAction)ChangeNumber:(id)sender;

- (IBAction)touchRightButton:(id)sender;
- (IBAction)touchLeftButton:(id)sender;

- (IBAction)pressLeftSpecialButton:(id)sender;
- (IBAction)pressRightSpecialButton:(id)sender;

@end
