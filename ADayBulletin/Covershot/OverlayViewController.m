//
//  OverlayViewController.m
//  aday
//
//  Created by Batt on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "OverlayViewController.h"
#import "AppDelegate.h"

@interface OverlayViewController()

- (void)cancelEdit;
//- (void)showEditButton:(BOOL)show;
- (void)showLeftButton:(BOOL)show;
- (void)showRightButton:(BOOL)show;
- (BOOL)checkPage;

@end

@implementation OverlayViewController

@synthesize enableediting , specialFilterScrollView , currentSpecialFilterIndex , rightSpecialButton , leftSpecialButton , numberLabel , changeNumberButton;

- (id)init
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self = [super initWithNibName:@"OverlayViewController" bundle:nil];
    }
    else
    {
        self = [super initWithNibName:@"OverlayViewController_iPhone" bundle:nil];
    }
    
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initOverlayWithSpecialMenu{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self = [super initWithNibName:@"OverlayViewController" bundle:nil];
    }
    else
    {
        self = [super initWithNibName:@"OverlayViewController_iPhone" bundle:nil];
    }
    
    if (self) {
        // Custom initialization
        self.isSpecialMenu = YES;
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//        specialMenuDictionary = appDelegate.specialMenu.specialMenuDictionary;
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self cancelEdit];

    //Manage NumpadViewController
    numpadVC = [[NumpadViewController alloc] init];
    numpadVC.delegate = self;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        numpadVC.view.frame = CGRectMake(434, 60, 250, 405);
    }
    else{
        numpadVC.view.frame = CGRectMake(56, 63, 208, 329);
    }
    [self.view addSubview:numpadVC.view];

    //Manage Gesture on ColorTabScrollView
    touch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchScrollView)];
	touch.numberOfTouchesRequired = 1;
    touch.numberOfTapsRequired = 1; 
    touch.delegate = self;
    [colorTabScrollView addGestureRecognizer:touch];
    colorTabScrollView.scrollEnabled = NO;
    
    //Add ColorTab on ColorTabScrollView
    for (int i=1; i<=8; i++)
    {
        UIImage *colorTab = [UIImage imageNamed:[NSString stringWithFormat:@"ColorTab%d.png",i]];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:colorTab];
        
        
        int imageW,imageH;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            imageW = 768;
            imageH = colorTab.size.height;
        }
        else
        {
            imageW = 320;
            imageH = 42;
        }

        imageView.frame = CGRectMake(imageW*(i-1) , 0, imageW, imageH);
        [colorTabScrollView addSubview:imageView];
        colorTabScrollView.contentSize = CGSizeMake(imageW*i, imageH);
    }
    
    [self checkPage];
    [self setEnableediting:NO];
    
    if(self.isSpecialMenu == YES){
//        [self createSpecialFilter];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)addFilter{
    [self.view addSubview:self.specialFilterScrollView];
    [self.view insertSubview:self.specialFilterScrollView atIndex:0];
    [leftSpecialButton setHidden:NO];
    [rightSpecialButton setHidden:NO];
}

/*-(void)createSpecialFilter{
    [numberLabel setText:[specialMenuDictionary objectForKey:@"bookno"]];

    [self.specialFilterScrollView setShowsHorizontalScrollIndicator:NO];
    [self.specialFilterScrollView setShowsVerticalScrollIndicator:NO];
    [self.specialFilterScrollView setFrame:CGRectMake(0, 0, self.specialFilterScrollView.frame.size.width, self.specialFilterScrollView.frame.size.height)];
    
    NSMutableArray *filterArray = [specialMenuDictionary objectForKey:@"filteriphone"];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        filterArray = [specialMenuDictionary objectForKey:@"filteripad"];
    }
    [self.specialFilterScrollView setContentSize:CGSizeMake(self.specialFilterScrollView.frame.size.width*[filterArray count], self.specialFilterScrollView.frame.size.height)];
    [self.specialFilterScrollView setPagingEnabled:YES];
    
    [self performSelector:@selector(addFilter) withObject:nil afterDelay:2.0];
    int count = 0;
    if([specialMenuDictionary objectForKey:@"filtercomponent"] == nil){
        NSMutableArray *filterComponentArray = [[NSMutableArray alloc] init];
        [specialMenuDictionary setObject:filterComponentArray forKey:@"filtercomponent"];
        for (NSString *filterImageLinkString in filterArray) {
            
            //Create ImageView
            UIImageView *filterImageView = [[UIImageView alloc] init];
            [filterImageView setContentMode:UIViewContentModeRedraw];
            [filterImageView setBackgroundColor:[UIColor clearColor]];
            [filterImageView setFrame:CGRectMake(count*self.specialFilterScrollView.frame.size.width, 0, self.specialFilterScrollView.frame.size.width, self.specialFilterScrollView.frame.size.height)];
            [self.specialFilterScrollView addSubview:filterImageView];
            
            //Create ActivityView
            UIActivityIndicatorView *filterLoadingView = [[UIActivityIndicatorView alloc] init];
            [filterLoadingView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
            [filterLoadingView setCenter:filterImageView.center];
            [filterLoadingView startAnimating];
            [filterLoadingView setHidden:NO];
            [filterLoadingView setBackgroundColor:[UIColor blueColor]];
            [self.specialFilterScrollView addSubview:filterLoadingView];
            
            NSMutableDictionary *filterInfo = [[NSMutableDictionary alloc] init];
            [filterInfo setObject:filterImageLinkString forKey:@"filterlinkstring"];
            [filterInfo setObject:filterImageView forKey:@"filterimageview"];
            [filterInfo setObject:filterLoadingView forKey:@"filterloadingview"];
            
            [filterComponentArray addObject:filterInfo];
            [self loadFilter:filterInfo AtIndex:count];
            
            count++;
        }

    }
    else{
        NSMutableArray *filterComponentArray = [specialMenuDictionary objectForKey:@"filtercomponent"];
        for (NSMutableDictionary *filterComponentItem in filterComponentArray) {
            UIImageView *filterImageView = [filterComponentItem objectForKey:@"filterimageview"];
            [self.specialFilterScrollView addSubview:filterImageView];
        }
    }
}

- (void)loadFilter:(NSMutableDictionary *)filterdic AtIndex:(int)indexint{
    
    NSString *filterLinkString = [filterdic objectForKey:@"filterlinkstring"];
    ASIHTTPRequest *httpRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:filterLinkString]];
    [httpRequest setDelegate:self];
    [httpRequest setTag:indexint];
    [httpRequest setDidFinishSelector:@selector(requestImageFinished:)];
    [httpRequest setDidFailSelector:@selector(requestImageFailed:)];
    [httpRequest startAsynchronous];
}

- (void)requestImageFinished:(ASIHTTPRequest *)request{
    if ([request responseStatusCode] == 200) {
        
        UIImage *image = [UIImage imageWithData:[request responseData]];
        NSMutableArray *filterArray = [specialMenuDictionary objectForKey:@"filtercomponent"];
        NSMutableDictionary *filterdic = [filterArray objectAtIndex:request.tag];
        
        UIImageView *filterImageView = [filterdic objectForKey:@"filterimageview"];
        UIActivityIndicatorView *filterLoadingView = [filterdic objectForKey:@"filterloadingview"];

        if(image != nil){
            [filterImageView setImage:image];
            [filterImageView setContentMode:UIViewContentModeRedraw];
            [filterImageView setFrame:CGRectMake(filterImageView.frame.origin.x, filterImageView.frame.origin.y, self.specialFilterScrollView.frame.size.width, image.size.width*self.specialFilterScrollView.frame.size.height/image.size.width)];
            [filterLoadingView setBackgroundColor:[UIColor redColor]];
            [filterLoadingView setHidden:YES];
        }
    }
}

- (void)requestImageFailed:(ASIHTTPRequest *)request{
    NSLog(@"request failed");
}
*/
- (void)setEnableediting:(BOOL)boo
{
    if (!boo){
        [colorTabScrollView removeGestureRecognizer:touch];
    }
    else{
        [colorTabScrollView addGestureRecognizer:touch];
    }
    
    enableTouchScrollView = boo;
    [self showEditButton:boo];
}
- (void)touchScrollView
{
    if (enableTouchScrollView) 
    {
        if (editColorTab) {
            [self cancelEdit];
        }
        else if (enableediting){
            [self showEditButton:NO];
        }
        else{
            [self showEditButton:YES];
        }
    }
    
    
    
}
#pragma mark - IBAction

- (IBAction)pressLeftSpecialButton:(id)sender{
    float currentOffsetX = self.specialFilterScrollView.contentOffset.x;
    float previoseOffsetX = currentOffsetX-self.specialFilterScrollView.frame.size.width;
    if(previoseOffsetX < 0){
        previoseOffsetX = self.specialFilterScrollView.contentSize.width-self.specialFilterScrollView.frame.size.width;
    }
    [specialFilterScrollView scrollRectToVisible:CGRectMake(previoseOffsetX, 0, self.specialFilterScrollView.frame.size.width, self.specialFilterScrollView.frame.size.height) animated:YES];
}

- (IBAction)pressRightSpecialButton:(id)sender{
    float currentOffsetX = self.specialFilterScrollView.contentOffset.x;
    float nextOffsetX = currentOffsetX+self.specialFilterScrollView.frame.size.width;
    if(nextOffsetX > self.specialFilterScrollView.contentSize.width-self.specialFilterScrollView.frame.size.width){
        nextOffsetX = 0;
    }
    [self.specialFilterScrollView scrollRectToVisible:CGRectMake(nextOffsetX, 0, self.specialFilterScrollView.frame.size.width, self.specialFilterScrollView.frame.size.height) animated:YES];
}

- (IBAction)ChangeColor:(id)sender
{
    editColorTab = YES;
    
    colorTabScrollView.scrollEnabled = YES;
    adayLogo.hidden = YES;
    leftButton.hidden = NO;
    rightButton.hidden = NO;
    
    changeNumberButton.hidden = YES;
    changeColorTabButton.hidden = YES;
    numberLabel.hidden = YES;
}

- (IBAction)ChangeNumber:(id)sender
{
    editColorTab = NO;
    
    if (numpadVC.view.hidden) {
        numpadVC.view.hidden = NO;
    }
    else{
        numpadVC.view.hidden = YES;
    }
    
}

- (IBAction)touchRightButton:(id)sender
{
    [colorTabScrollView setContentOffset:CGPointMake(colorTabScrollView.contentOffset.x+colorTabScrollView.frame.size.width, 0) 
                                animated:YES];
}

- (IBAction)touchLeftButton:(id)sender
{
    [colorTabScrollView setContentOffset:CGPointMake(colorTabScrollView.contentOffset.x-colorTabScrollView.frame.size.width, 0) 
                                animated:YES];
}

#pragma mark -

- (void)cancelEdit
{
    editColorTab = NO;
    changeColorTabButton.hidden = NO;
    colorTabScrollView.scrollEnabled = NO;
    adayLogo.hidden = NO;
    leftButton.hidden = YES;
    rightButton.hidden = YES;
    changeNumberButton.hidden = NO;
    numberLabel.hidden = NO;
    
    if(self.isSpecialMenu == YES){
        [self.changeNumberButton setHidden:YES];
    }
}

- (void)showEditButton:(BOOL)show
{
    if (show) {
        enableediting = YES;
        changeNumberButton.hidden = NO;
        changeColorTabButton.hidden = NO;
    }
    else{
        enableediting = NO;
        changeNumberButton.hidden = YES;
        changeColorTabButton.hidden = YES;
        numpadVC.view.hidden = YES;
    }
    if(self.isSpecialMenu == YES){
        [self.changeNumberButton setHidden:YES];
    }
}


#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scroll
{
    if(scroll != self.specialFilterScrollView){
        [self checkPage];
    }
}

-(int)getCurrentFilterIndex{
    return self.specialFilterScrollView.contentOffset.x/self.specialFilterScrollView.frame.size.width;
}

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    if(scrollView == self.specialFilterScrollView){
//        self.currentSpecialFilterIndex = scrollView.contentOffset.x/scrollView.frame.size.width;
//    }
//}

- (void)checkPage
{
    CGFloat pageWidth = colorTabScrollView.frame.size.width;
    CGFloat contentWidth = colorTabScrollView.contentSize.width;
    
    int page = floor((colorTabScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    if ( (contentWidth - (pageWidth*page)) > pageWidth) 
    {
        [self showRightButton:YES];
    }
    else{
        [self showRightButton:NO];
    }
    
    if (colorTabScrollView.contentOffset.x <= pageWidth/2) {
        [self showLeftButton:NO];
    }
    else{
        [self showLeftButton:YES];
    }
}

- (void)showLeftButton:(BOOL)show
{
    if (show) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        leftButton.alpha = 1.0;
        [UIView commitAnimations];
    }
    else{
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        leftButton.alpha = 0.0;
        [UIView commitAnimations];
    }

}

- (void)showRightButton:(BOOL)show
{
    if (show) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        rightButton.alpha = 1.0;
        [UIView commitAnimations];
    }
    else{
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.4];
        rightButton.alpha = 0.0;
        [UIView commitAnimations];
    }
}

#pragma mark - NumpadDelegate
- (void)doneEditNumber:(NSString *)str
{
    numberLabel.text = str;
    [self ChangeNumber:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
