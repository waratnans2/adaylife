//
//  ShareCoverShootViewController.h
//  aday
//
//  Created by Batt on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "FacebookController.h"
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>

@protocol ShareCoverShootViewControllerDelegate
- (IBAction)dismissView:(id)sender;
@end

@interface ShareCoverShootViewController : UIViewController
{
    IBOutlet UIImageView *resultImageView;
    IBOutlet UIView *shareView;
    IBOutlet UILabel *titlePopup;
    IBOutlet UITextField *message;
    IBOutlet UIView *popupView;
}
- (id)initWithImage:(UIImage*)image;

@property (assign) UIImage *resultImage;
@property (nonatomic, assign) id<ShareCoverShootViewControllerDelegate, NSObject> delegate;

- (IBAction)dissmissView:(id)sender;
- (IBAction)touchShareFacebook:(id)sender;
- (IBAction)touchShareTwitter:(id)sender;
- (IBAction)touchShare:(id)sender;

- (IBAction)touchSend:(id)sender;
- (IBAction)touchClose:(id)sender;

@end
