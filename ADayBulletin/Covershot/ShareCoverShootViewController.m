//
//  ShareCoverShootViewController.m
//  aday
//
//  Created by Batt on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ShareCoverShootViewController.h"

@interface ShareCoverShootViewController()
- (void)performTWRequestUpload;
- (void)showPopup:(NSString*)title;


@end

@implementation ShareCoverShootViewController
@synthesize resultImage;
@synthesize delegate;
- (id)initWithImage:(UIImage*)image
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self = [super initWithNibName:@"ShareCoverShootViewController" bundle:nil];
    }
    else
    {
        self = [super initWithNibName:@"ShareCoverShootViewController_iPhone" bundle:nil];
    }

    //ShareCoverShootViewController_iPhone
    if (self) {
//        resultImage = [[UIImage alloc] init];
        resultImage = image;
    }
    return self;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    shareView.hidden = YES;
    resultImageView.image = resultImage;

    [self touchClose:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [delegate dismissView:nil];

}
- (IBAction)touchShareFacebook:(id)sender
{
    [self showPopup:@"Facebook"];
    [self touchShare:nil];
}

- (IBAction)touchShareTwitter:(id)sender
{
    [self showPopup:@"Twitter"];
    [self touchShare:nil];
}

- (IBAction)touchShare:(id)sender
{
    shareView.hidden = !shareView.hidden;
}

- (IBAction)dissmissView:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma - mark TWITTER
- (void)performTWRequestUpload
{
    NSLog(@"streamTwitter");    
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
	
	// Create an account type that ensures Twitter accounts are retrieved.
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
	
	// Request access from the user to use their Twitter accounts.
    [accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error) {
        if(granted) 
        {
			// Get the list of Twitter accounts.
            NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
			
			// For the sake of brevity, we'll assume there is only one Twitter account present.
			// You would ideally ask the user which account they want to tweet from, if there is more than one Twitter account present.
			if ([accountsArray count] > 0) 
            {
				
                NSURL *url = 
                [NSURL URLWithString:
                 @"https://upload.twitter.com/1/statuses/update_with_media.json"];
                
                TWRequest *request = 
                [[TWRequest alloc] initWithURL:url parameters:nil 
                                 requestMethod:TWRequestMethodPOST];
                
                [request setAccount:[accountsArray objectAtIndex:0]];

                NSData *imageData = UIImagePNGRepresentation(resultImage);
                
                //	correct parameter name, "media[]"
                [request addMultiPartData:imageData 
                                 withName:@"media[]" type:@"multipart/form-data"];
                
//                NSString *status = @"a day on iPad";
                NSString *status = message.text;
                
                [request addMultiPartData:[status dataUsingEncoding:NSUTF8StringEncoding] 
                                 withName:@"status" type:@"multipart/form-data"];
                
                [request performRequestWithHandler:
                 ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                     NSDictionary *dict = 
                     (NSDictionary *)[NSJSONSerialization 
                                      JSONObjectWithData:responseData options:0 error:nil];
                     
//                     NSLog(@"%@", dict);
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         NSLog(@"Post FIN");
                     });
                 }];

            
            }
        }
	}];
    
}


#pragma  mark - POP UP

- (void)showPopup:(NSString*)title
{
    titlePopup.text = title;
    popupView.hidden = NO;
    [message becomeFirstResponder];

}

- (IBAction)touchSend:(id)sender
{
    if ([titlePopup.text isEqualToString:@"Facebook"]) 
    {
//        FacebookController *fbControl = [[FacebookController alloc] init];
//        [fbControl postImage:resultImage withText:message.text];
//        [fbControl checkLogin];
    }
    else if ([titlePopup.text isEqualToString:@"Twitter"]) 
    {
        [self performTWRequestUpload];
    }
        
    [self touchClose:nil];
}

- (IBAction)touchClose:(id)sender
{
    popupView.hidden = YES;
    [message resignFirstResponder];

}


#pragma mark -

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (interfaceOrientation == UIInterfaceOrientationPortrait ||
        interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        return YES;
    }
    else{
        return NO;
    }}

@end
