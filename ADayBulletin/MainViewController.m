//
//  MainViewController.m
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/17/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//

#import "MainViewController.h"
#import "Book.h"
#import "DBManager.h"
#import "ApiUtils.h"
#import "CoverShootViewConroller.h"
@interface MainViewController ()
{
    ApiUtils * myApi;

}
@property (nonatomic,retain) DBManager * dbMgr;

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(DBManager*)dbMgr
{
    if(_dbMgr == nil){
        _dbMgr = [[DBManager alloc]init];
    }
    return _dbMgr;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    

    myApi = [[ApiUtils alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveDownloadFileFinishNotification:)
                                                 name:@"finishDownloadPDFFileForMainNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveDownloadTempFileNotification:)
                                                 name:@"finishDownloadTempFileForMainNotification"
                                               object:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self resumeDownload];

}

-(void)resumeDownload
{
    
    NSMutableArray * bookForResume = [self.dbMgr fetchBook:@"state !=3"];
    NSLog(@"book for resume %d",[bookForResume count]);
    
    for (Book *bookObj in bookForResume) {
        
        NSString* downloadPDFFileNoti = [NSString stringWithFormat:@"finishDownloadPDFFileForBook%@",bookObj.bookName];
        NSString * downloadTempFileNoti = [NSString stringWithFormat:@"finishDownloadTempFileForBook%@",bookObj.bookName];

        int pageCount = [bookObj.pageCount intValue];
        if([bookObj.state isEqualToString:@"1"]){
            
            for (int i = 1; i<= pageCount; i++) {
                [myApi downloadTempFile:bookObj.filePath  year:bookObj.year fileName:bookObj.tempFile index:i notificationName:downloadTempFileNoti book:bookObj];
            }
        }
        
        int bookDownloaded = ([bookObj.pageDownloaded intValue]>1)?[bookObj.pageDownloaded intValue]:1;
        
        for (int i = bookDownloaded; i<= pageCount; i++) {
            [myApi downloadFile:bookObj.filePath year:bookObj.year fileName:bookObj.fileName index:i notificationName:downloadPDFFileNoti book:bookObj];
        }
        
        [myApi nextOperation];
    }
}


#pragma mark notification
- (void) receiveDownloadFileFinishNotification:(NSNotification *) notification
{
    
    if([notification.name isEqualToString:@"finishDownloadPDFFileForMainNotification"]){

        NSDictionary* userInfo = notification.userInfo;
        
        int pageIndex = [[userInfo valueForKey:@"pageIndex"] integerValue];
        Book *bookObj = [userInfo valueForKey:@"Book"];
    
        NSLog(@"MAIN Download PDF %d of %@ ",pageIndex, bookObj.pageCount);

        bookObj.pageDownloaded = [NSString stringWithFormat:@"%d",pageIndex];
    
        if(pageIndex == [bookObj.pageCount intValue]){
            bookObj.state = [NSString stringWithFormat:@"3"];
        }
        [self.dbMgr updateDBBook:bookObj];
    }
}

- (void) receiveDownloadTempFileNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    
    if([notification.name isEqualToString:@"finishDownloadTempFileForMainNotification"]){
        
        NSDictionary* userInfo = notification.userInfo;
        
        int pageIndex = [[userInfo valueForKey:@"pageIndex"] integerValue];
        Book *bookObj = [userInfo valueForKey:@"Book"];
        
        NSLog(@"MAIN Download %d of %@ ",pageIndex, bookObj.pageCount);

        if(pageIndex == [bookObj.pageCount intValue]){
            //downloading state
            bookObj.state = [NSString stringWithFormat:@"2"];
            [self.dbMgr updateDBBook:bookObj];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


//
//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
//{
//	if (isVisible == NO) return; // iOS present modal bodge
//    
//	if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
//	{
//		if (printInteraction != nil) [printInteraction dismissAnimated:NO];
//	}
//}
//
-(IBAction)takePicture:(id)sender{
    NSLog(@"Take Picture");
    CoverShootViewConroller * coverShootVC = [[CoverShootViewConroller alloc] init];
    [self.navigationController pushViewController:coverShootVC animated:YES];
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}
@end
