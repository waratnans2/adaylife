//
//	ReaderContentView.m
//	Reader v2.7.3
//
//	Created by Julius Oklamcak on 2011-07-01.
//	Copyright © 2011-2013 Julius Oklamcak. All rights reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy
//	of this software and associated documentation files (the "Software"), to deal
//	in the Software without restriction, including without limitation the rights to
//	use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
//	of the Software, and to permit persons to whom the Software is furnished to
//	do so, subject to the following conditions:
//
//	The above copyright notice and this permission notice shall be included in all
//	copies or substantial portions of the Software.
//
//	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
//	OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//	CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


#import "ReaderConstants.h"
#import "ReaderContentView.h"
#import "ReaderContentPage.h"
#import "ReaderThumbCache.h"

#import <QuartzCore/QuartzCore.h>

@interface ReaderContentView () <UIScrollViewDelegate>
{
    UIActivityIndicatorView *loading;
}

@end

@implementation ReaderContentView
{
	ReaderContentPage *theContentView;

	ReaderContentThumb *theThumbView;
    //test m
    ReaderContentPage *theContentView1;
	ReaderContentThumb *theThumbView1;

	UIView *theContainerView;
}

#pragma mark Constants

#define ZOOM_FACTOR 2.0f
#define ZOOM_MAXIMUM 4.0f

#if (READER_SHOW_SHADOWS == TRUE) // Option
	#define CONTENT_INSET 4.0f
#else
	#define CONTENT_INSET 2.0f
#endif // end of READER_SHOW_SHADOWS Option

#define PAGE_THUMB_LARGE 240
#define PAGE_THUMB_SMALL 144

static void *ReaderContentViewContext = &ReaderContentViewContext;

#pragma mark Properties

@synthesize message;

#pragma mark ReaderContentView functions
//
//static inline CGFloat ZoomScaleThatFits(CGSize target, CGSize source)
//{
//	CGFloat w_scale = (target.width / source.width);
//
//	CGFloat h_scale = (target.height / source.height);
//
//	return ((w_scale < h_scale) ? w_scale : h_scale);
//}
static inline CGFloat ZoomScaleThatFits(CGSize target, CGSize source)
{
	CGFloat w_scale; UIInterfaceOrientation orientation= [[UIApplication sharedApplication] statusBarOrientation];
    
    if(UIInterfaceOrientationIsLandscape(orientation)&&[UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        w_scale= ((target.width/2) / source.width);
    }else{
        if(target.width < source.width){
            w_scale= (target.width / source.width);
        }else{
            w_scale=1;
        }
    }
    return w_scale;
}

#pragma mark ReaderContentView instance methods
- (void)updateMinimumMaximumZoom
{
	CGRect targetRect = CGRectInset(self.bounds, CONTENT_INSET, CONTENT_INSET);

	CGFloat zoomScale = ZoomScaleThatFits(targetRect.size, theContentView.bounds.size);

	self.minimumZoomScale = zoomScale; // Set the minimum and maximum zoom scales

	self.maximumZoomScale = (zoomScale * ZOOM_MAXIMUM); // Max number of zoom levels
}

- (id)initWithFrame:(CGRect)frame fileURL:(NSURL *)fileURL page:(NSUInteger)page password:(NSString *)phrase
{
	if ((self = [super initWithFrame:frame]))
	{
		self.scrollsToTop = NO;
		self.delaysContentTouches = NO;
		self.showsVerticalScrollIndicator = NO;
		self.showsHorizontalScrollIndicator = NO;
		self.contentMode = UIViewContentModeRedraw;
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		self.backgroundColor = [UIColor clearColor];
		self.userInteractionEnabled = YES;
		self.autoresizesSubviews = NO;
		self.bouncesZoom = YES;
		self.delegate = self;

//		theContentView = [[ReaderContentPage alloc] initWithURL:fileURL page:page password:phrase];
//    
//		if (theContentView != nil) // Must have a valid and initialized content view
//		{
//			theContainerView = [[UIView alloc] initWithFrame:theContentView.bounds];
//
//			theContainerView.autoresizesSubviews = NO;
//			theContainerView.userInteractionEnabled = NO;
//			theContainerView.contentMode = UIViewContentModeRedraw;
//			theContainerView.autoresizingMask = UIViewAutoresizingNone;
//			theContainerView.backgroundColor = [UIColor whiteColor];
//
//#if (READER_SHOW_SHADOWS == TRUE) // Option
//
//			theContainerView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
//			theContainerView.layer.shadowRadius = 4.0f; theContainerView.layer.shadowOpacity = 1.0f;
//			theContainerView.layer.shadowPath = [UIBezierPath bezierPathWithRect:theContainerView.bounds].CGPath;
//
//#endif // end of READER_SHOW_SHADOWS Option
//
//			self.contentSize = theContentView.bounds.size; // Content size same as view size
//			self.contentOffset = CGPointMake((0.0f - CONTENT_INSET), (0.0f - CONTENT_INSET)); // Offset
//			self.contentInset = UIEdgeInsetsMake(CONTENT_INSET, CONTENT_INSET, CONTENT_INSET, CONTENT_INSET);
//
//#if (READER_ENABLE_PREVIEW == TRUE) // Option
//
//			theThumbView = [[ReaderContentThumb alloc] initWithFrame:theContentView.bounds]; // Page thumb view
//
//			[theContainerView addSubview:theThumbView]; // Add the thumb view to the container view
//
//#endif // end of READER_ENABLE_PREVIEW Option
//
//			[theContainerView addSubview:theContentView]; // Add the content view to the container view
//
//			[self addSubview:theContainerView]; // Add the container view to the scroll view
//
//			[self updateMinimumMaximumZoom]; // Update the minimum and maximum zoom scales
//
//			self.zoomScale = self.minimumZoomScale; // Set zoom to fit page content
//            
//		}else{
//            //test m
//            
//            UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc]init];
//            loading.frame = CGRectMake(100, 100, 120, 120);
//            [loading startAnimating];
//            [self addSubview:loading];
//
//        }
//        
//		[self addObserver:self forKeyPath:@"frame" options:0 context:ReaderContentViewContext];
//
//		self.tag = page; // Tag the view with the page number
//	}
        [self renderContentfileURL:fileURL page:page password:phrase];
        [self addObserver:self forKeyPath:@"frame" options:0 context:ReaderContentViewContext];
        self.tag = page; // Tag the view with the page number
        
    }
	return self;
}

//test m
-(void)renderContentfileURL:(NSURL *)fileURL page:(NSUInteger)page password:(NSString *)phrase
{
    theContentView = [[ReaderContentPage alloc] initWithURL:fileURL page:page password:phrase];
    if (theContentView != nil) // Must have a valid and initialized content view
    {
        theContainerView = [[UIView alloc] initWithFrame:theContentView.bounds];
        theContainerView.autoresizesSubviews = NO;
        theContainerView.userInteractionEnabled = NO;
        theContainerView.contentMode = UIViewContentModeRedraw;
        theContainerView.autoresizingMask = UIViewAutoresizingNone;
        theContainerView.backgroundColor = [UIColor whiteColor];
        
#if (READER_SHOW_SHADOWS == TRUE) // Option
        
        theContainerView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
        theContainerView.layer.shadowRadius = 4.0f; theContainerView.layer.shadowOpacity = 1.0f;
        theContainerView.layer.shadowPath = [UIBezierPath bezierPathWithRect:theContainerView.bounds].CGPath;
        
#endif // end of READER_SHOW_SHADOWS Option
        
        self.contentSize = theContentView.bounds.size; // Content size same as view size
        self.contentOffset = CGPointMake((0.0f - CONTENT_INSET), (0.0f - CONTENT_INSET)); // Offset
        self.contentInset = UIEdgeInsetsMake(CONTENT_INSET, CONTENT_INSET, CONTENT_INSET, CONTENT_INSET);
        
#if (READER_ENABLE_PREVIEW == TRUE) // Option
        
        theThumbView = [[ReaderContentThumb alloc] initWithFrame:theContentView.bounds]; // Page thumb view
        
        [theContainerView addSubview:theThumbView]; // Add the thumb view to the container view
        
#endif // end of READER_ENABLE_PREVIEW Option
        
        [theContainerView addSubview:theContentView]; // Add the content view to the container view
        
        [self addSubview:theContainerView]; // Add the container view to the scroll view
        
        [self updateMinimumMaximumZoom]; // Update the minimum and maximum zoom scales
        
        self.zoomScale = self.minimumZoomScale; // Set zoom to fit page content
        
    }else{
        //test m
        
        loading = [[UIActivityIndicatorView alloc]init];
        loading.frame = CGRectMake(100, 100, 120, 120);
        [loading startAnimating];
        
        [self addSubview:loading];
        CGRect bounds = loading.superview.bounds;
        loading.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
        
    }
    
}

-(void)renderLanscapeContentDocument:(ReaderDocument *)document page:(NSUInteger)page password:(NSString *)phrase maxPage:(NSInteger)maxpage
{
    int docPage = (page == 0)? 1: page;
    
    NSURL *fileURL1 = [[NSURL alloc]initFileURLWithPath:[document.filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.pdf",docPage,document.pdfFileName]]];
    
    theContentView = [[ReaderContentPage alloc] initWithURL:fileURL1 page:page password:phrase];
    theContentView.frame=CGRectMake(theContentView.frame.origin.x, theContentView.frame.origin.y,theContentView.frame.size.width/2, theContentView.frame.size.height/2);
    
    if(docPage!=1 ){
        
        NSURL *fileURL2 = [[NSURL alloc]initFileURLWithPath:[document.filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.pdf",page+1,document.pdfFileName]]];

        theContentView1=[[ReaderContentPage alloc]initWithURL:fileURL2 page:page password:phrase];
        theContentView1.frame=CGRectMake(theContentView.frame.size.width, theContentView.frame.origin.y,theContentView.frame.size.width, theContentView.frame.size.height);
    [theContentView1 setBackgroundColor:[UIColor redColor]];
    }

    if ((theContentView != nil && theContentView1 != nil) || (page == maxpage && theContentView != nil) || page == 0) // Must have a valid and initialized content view
    {
        theContainerView = [[UIView alloc] initWithFrame:CGRectMake(theContentView.frame.origin.x, theContentView.frame.origin.y, theContentView.frame.size.width*2, theContentView.frame.size.height)];
        
        theContainerView.autoresizesSubviews = NO;
        theContainerView.userInteractionEnabled = NO;
        theContainerView.contentMode = UIViewContentModeRedraw;
        theContainerView.autoresizingMask = UIViewAutoresizingNone;
        theContainerView.backgroundColor = [UIColor whiteColor];
        
        if(theContentView1 == nil){
            
            [theContainerView setBackgroundColor:[UIColor clearColor]];
            theContainerView.frame = theContentView.frame;
            
        }
        
#if (READER_SHOW_SHADOWS == TRUE) // Option
        
        theContainerView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
        theContainerView.layer.shadowRadius = 4.0f; theContainerView.layer.shadowOpacity = 1.0f;
        theContainerView.layer.shadowPath = [UIBezierPath bezierPathWithRect:theContainerView.bounds].CGPath;
        
#endif // end of READER_SHOW_SHADOWS Option
        
        self.contentSize = theContentView.bounds.size; // Content size same as view size
        self.contentOffset = CGPointMake((0.0f - CONTENT_INSET), (0.0f - CONTENT_INSET)); // Offset
        self.contentInset = UIEdgeInsetsMake(CONTENT_INSET, CONTENT_INSET, CONTENT_INSET, CONTENT_INSET);
        
#if (READER_ENABLE_PREVIEW == TRUE) // Option
        
        theThumbView = [[ReaderContentThumb alloc] initWithFrame:theContentView.bounds]; // Page thumb view
        theThumbView1=[[ReaderContentThumb alloc] initWithFrame:theContentView1.frame];
        
        [theContainerView addSubview:theThumbView]; // Add the thumb view to the container view
        [theContainerView addSubview:theThumbView1];
#endif // end of READER_ENABLE_PREVIEW Option
        
        [theContainerView addSubview:theContentView];
        [theContainerView addSubview:theContentView1];// Add the content view to the container view
        
        [self addSubview:theContainerView]; // Add the container view to the scroll view
        
        [self updateMinimumMaximumZoom]; // Update the minimum and maximum zoom scales
        
        self.zoomScale = self.minimumZoomScale; // Set zoom to fit page content
        
    }else{
        
        UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc]init];
        loading.frame = CGRectMake(100, 100, 120, 120);
        [loading startAnimating];
        
        [self addSubview:loading];
        CGRect bounds = loading.superview.bounds;
        loading.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    }
}

//test m
- (id)initWithFrameLandscape:(CGRect)frame document:(ReaderDocument *)document page:(NSUInteger)page password:(NSString *)phrase maxPage:(NSInteger)maxpage
{
	if ((self = [super initWithFrame:frame]))
	{
		self.scrollsToTop = NO;
		self.delaysContentTouches = NO;
		self.showsVerticalScrollIndicator = NO;
		self.showsHorizontalScrollIndicator = NO;
		self.contentMode = UIViewContentModeRedraw;
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		self.backgroundColor = [UIColor clearColor];
		self.userInteractionEnabled = YES;
		self.autoresizesSubviews = NO;
		self.bouncesZoom = YES;
		self.delegate = self;
        
//        NSURL *fileURL1 = [[NSURL alloc]initFileURLWithPath:[document.filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.pdf",page,document.bookName]]];
//        
//		theContentView = [[ReaderContentPage alloc] initWithURL:fileURL1 page:page password:phrase];
//        theContentView.frame=CGRectMake(theContentView.frame.origin.x, theContentView.frame.origin.y,theContentView.frame.size.width/2, theContentView.frame.size.height/2);
//		
//        if((page+1)<=maxpage)
//        {
//            NSURL *fileURL2 = [[NSURL alloc]initFileURLWithPath:[document.filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.pdf",page+1,document.bookName]]];
//            theContentView1=[[ReaderContentPage alloc]initWithURL:fileURL2 page:page password:phrase];
//            theContentView1.frame=CGRectMake(theContentView.frame.size.width, theContentView.frame.origin.y,            theContentView.frame.size.width, theContentView.frame.size.height);
//        }
//
//        if (theContentView1 != nil ) // Must have a valid and initialized content view
//		{
//            
//			theContainerView = [[UIView alloc] initWithFrame:CGRectMake(theContentView.frame.origin.x, theContentView.frame.origin.y, theContentView.frame.size.width*2, theContentView.frame.size.height)];
//            
//			theContainerView.autoresizesSubviews = NO;
//			theContainerView.userInteractionEnabled = NO;
//			theContainerView.contentMode = UIViewContentModeRedraw;
//			theContainerView.autoresizingMask = UIViewAutoresizingNone;
//			theContainerView.backgroundColor = [UIColor whiteColor];
//            
//#if (READER_SHOW_SHADOWS == TRUE) // Option
//            
//			theContainerView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
//			theContainerView.layer.shadowRadius = 4.0f; theContainerView.layer.shadowOpacity = 1.0f;
//			theContainerView.layer.shadowPath = [UIBezierPath bezierPathWithRect:theContainerView.bounds].CGPath;
//            
//#endif // end of READER_SHOW_SHADOWS Option
//            
//			self.contentSize = theContentView.bounds.size; // Content size same as view size
//			self.contentOffset = CGPointMake((0.0f - CONTENT_INSET), (0.0f - CONTENT_INSET)); // Offset
//			self.contentInset = UIEdgeInsetsMake(CONTENT_INSET, CONTENT_INSET, CONTENT_INSET, CONTENT_INSET);
//            
//#if (READER_ENABLE_PREVIEW == TRUE) // Option
//            
//			theThumbView = [[ReaderContentThumb alloc] initWithFrame:theContentView.bounds]; // Page thumb view
//            theThumbView1=[[ReaderContentThumb alloc] initWithFrame:theContentView1.frame];
//            
//			[theContainerView addSubview:theThumbView]; // Add the thumb view to the container view
//            [theContainerView addSubview:theThumbView1];
//#endif // end of READER_ENABLE_PREVIEW Option
//            
//			[theContainerView addSubview:theContentView];
//            [theContainerView addSubview:theContentView1];// Add the content view to the container view
//            
//			[self addSubview:theContainerView]; // Add the container view to the scroll view
//            
//			[self updateMinimumMaximumZoom]; // Update the minimum and maximum zoom scales
//            
//			self.zoomScale = self.minimumZoomScale; // Set zoom to fit page content
//            
//		}else{
//            
//            UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc]init];
//            loading.frame = CGRectMake(100, 100, 120, 120);
//            [loading startAnimating];
//            [self addSubview:loading];
//        }
        
        [self renderLanscapeContentDocument:document page:page password:phrase maxPage:maxpage];
		[self addObserver:self forKeyPath:@"frame" options:0 context:ReaderContentViewContext];
        
		self.tag = page; // Tag the view with the page number
	}
    
	return self;
}

- (void)dealloc
{
	[self removeObserver:self forKeyPath:@"frame" context:ReaderContentViewContext];
}

- (void)showPageThumb:(ReaderDocument *)document page:(NSInteger)page password:(NSString *)phrase guid:(NSString *)guid
{
#if (READER_ENABLE_PREVIEW == TRUE) // Option

    //test m
    NSString *filePath = [document.filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.png",page,document.tempFile]];
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
	if ([image isKindOfClass:[UIImage class]]) [theThumbView showImage:image]; // Show image from cache

#endif // end of READER_ENABLE_PREVIEW Option
}
- (void)showPageThumbForTwo:(ReaderDocument *)document page:(NSInteger)page password:(NSString *)phrase guid:(NSString *)guid
{
#if (READER_ENABLE_PREVIEW == TRUE) // Option
    
	NSString *filePath = [document.filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.png",page,document.tempFile]];
    NSString *filePath2 = [document.filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.png",page+1,document.tempFile]];
    
    UIImage *image1 = [UIImage imageWithContentsOfFile:filePath];
    UIImage *image2 = [UIImage imageWithContentsOfFile:filePath2];
    
    [theThumbView showImage:image1];
    [theThumbView1 showImage:image2];
   
#endif // end of READER_ENABLE_PREVIEW Option
}
-(UIImage*)mergImage:(UIImage*)img1 image2:(UIImage*)img2
{
    
    CGSize size = CGSizeMake(img1.size.width*2,img1.size.height);
    
    UIGraphicsBeginImageContext(size);
    
    [img1 drawInRect:CGRectMake(0,0,img1.size.width, size.height)];
    [img2 drawInRect:CGRectMake(0,img1.size.width,img2.size.width, size.height)];
    
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return finalImage;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (context == ReaderContentViewContext) // Our context
	{
		if ((object == self) && [keyPath isEqualToString:@"frame"])
		{
			CGFloat oldMinimumZoomScale = self.minimumZoomScale;

			[self updateMinimumMaximumZoom]; // Update zoom scale limits

			if (self.zoomScale == oldMinimumZoomScale) // Old minimum
			{
				self.zoomScale = self.minimumZoomScale;
			}
			else // Check against minimum zoom scale
			{
				if (self.zoomScale < self.minimumZoomScale)
				{
					self.zoomScale = self.minimumZoomScale;
				}
				else // Check against maximum zoom scale
				{
					if (self.zoomScale > self.maximumZoomScale)
					{
						self.zoomScale = self.maximumZoomScale;
					}
				}
			}
		}
	}
}

- (void)layoutSubviews
{
	[super layoutSubviews];

	CGSize boundsSize = self.bounds.size;
	CGRect viewFrame = theContainerView.frame;

	if (viewFrame.size.width < boundsSize.width)
		viewFrame.origin.x = (((boundsSize.width - viewFrame.size.width) / 2.0f) + self.contentOffset.x);
	else
		viewFrame.origin.x = 0.0f;

	if (viewFrame.size.height < boundsSize.height)
		viewFrame.origin.y = (((boundsSize.height - viewFrame.size.height) / 2.0f) + self.contentOffset.y);
	else
		viewFrame.origin.y = 0.0f;
    
	theContainerView.frame = viewFrame;
    CGRect bounds = loading.superview.bounds;
    loading.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
}

- (id)processSingleTap:(UITapGestureRecognizer *)recognizer
{
	return [theContentView processSingleTap:recognizer];
}

- (void)zoomIncrement
{
	CGFloat zoomScale = self.zoomScale;
    NSLog(@"ZOOM SACLE %f max %f",self.zoomScale , self.maximumZoomScale);
	if (zoomScale < self.maximumZoomScale)
	{
		zoomScale *= ZOOM_FACTOR; // Zoom in

		if (zoomScale > self.maximumZoomScale)
		{
			zoomScale = self.maximumZoomScale;
		}

		[self setZoomScale:zoomScale animated:YES];
	}else{
        [self zoomResetWithAnimated];
    }
}

- (void)zoomDecrement
{
	CGFloat zoomScale = self.zoomScale;

	if (zoomScale > self.minimumZoomScale)
	{
		zoomScale /= ZOOM_FACTOR; // Zoom out

		if (zoomScale < self.minimumZoomScale)
		{
			zoomScale = self.minimumZoomScale;
		}

		[self setZoomScale:zoomScale animated:YES];
	}
}
- (void)zoomResetWithAnimated
{
    [self setZoomScale:self.minimumZoomScale animated:YES];
}

- (void)zoomReset
{
	if (self.zoomScale > self.minimumZoomScale)
	{
		self.zoomScale = self.minimumZoomScale;
	}
}

#pragma mark UIScrollViewDelegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
	return theContainerView;
}

#pragma mark UIResponder instance methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event]; // Message superclass

	[message contentView:self touchesBegan:touches]; // Message delegate
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesCancelled:touches withEvent:event]; // Message superclass
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesEnded:touches withEvent:event]; // Message superclass
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesMoved:touches withEvent:event]; // Message superclass
}

@end

#pragma mark -

//
//	ReaderContentThumb class implementation
//

@implementation ReaderContentThumb

#pragma mark ReaderContentThumb instance methods

- (id)initWithFrame:(CGRect)frame
{
	if ((self = [super initWithFrame:frame])) // Superclass init
	{
		imageView.contentMode = UIViewContentModeScaleAspectFill;

		imageView.clipsToBounds = YES; // Needed for aspect fill
	}

	return self;
}

@end
