//
//  main.m
//  ADayBulletin
//
//  Created by Waratnan Suriyasorn on 4/17/2557 BE.
//  Copyright (c) 2557 SiamSquared. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
